# ChessCoach
An Android application for chess trainers. It's used as a tool for setting up chess training positions for pupils to solve.
The tool enables easier communication  between a trainer and its student - trainers can easily create diagrams with tactics, then send them to students and see if
they solved them.
