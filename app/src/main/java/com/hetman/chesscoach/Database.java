package com.hetman.chesscoach;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.hetman.chesscoach.menus.DatabaseItem;
import com.hetman.chesscoach.services.DatabaseConnector;
import com.hetman.chesscoach.values.DatabaseModel;

import java.util.ArrayList;

public class Database extends AppCompatActivity
{
    DatabaseConnector connector;
    ListView lv;
    ArrayList<DatabaseModel> databaseModelArrayList = new ArrayList<>();
    ImageView filterButton;
    Cursor c;

    AlertDialog.Builder builder;
    DatabaseItem adapter;

    ArrayList<Integer> selectedLevelsList = new ArrayList<>();
    ArrayList<String> selectedLevelsStrings = new ArrayList<>();
    String [] levels = {"Amateur","Novice","Learner","Skilled","Expert"};
    boolean [] isSelectedLevelArray = {false, false, false, false, false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        Toolbar toolbar = (Toolbar) findViewById(R.id.database_toolbar);

        setSupportActionBar(toolbar);

        lv = (ListView) findViewById(R.id.database_list);
        filterButton = (ImageView) findViewById(R.id.filter_button);
        filterButton.setOnClickListener(filterClickListener);

        builder = new AlertDialog.Builder(this);

        connector = new DatabaseConnector(this);
        connector.openDB();
        c = connector.getAll();

        if(c.moveToFirst())
        {
            while(!c.isAfterLast())
            {
                DatabaseModel model = new DatabaseModel();
                model.setId(c.getLong(c.getColumnIndex("_id")));
                model.setName(c.getString(c.getColumnIndex("name")));
                model.setOnMove(c.getString(c.getColumnIndex("onmove")));
                model.setMoveNr(c.getInt(c.getColumnIndex("movenr")));
                model.setQuestion(c.getString(c.getColumnIndex("question")));
                model.setHint(c.getString(c.getColumnIndex("hint")));
                model.setLevel(c.getString(c.getColumnIndex("level")));
                model.setAnswer(c.getString(c.getColumnIndex("answer")));
                model.setTimer(c.getString(c.getColumnIndex("timer")));
                model.setFen(c.getString(c.getColumnIndex("fen")));
                model.setPgn(c.getString(c.getColumnIndex("pgn")));

                databaseModelArrayList.add(model);
                c.moveToNext();
            }
            c.close();
        }

        adapter = new DatabaseItem(this,R.layout.list_item_database,databaseModelArrayList);
        lv.setAdapter(adapter);
    }

    private View.OnClickListener filterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            builder.setTitle("Filter by level");
            builder.setMultiChoiceItems(R.array.levels, isSelectedLevelArray, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int iter, boolean isChecked) {
                    if(isChecked)
                    {
                        selectedLevelsStrings.add(levels[iter]);
                        isSelectedLevelArray[iter] = true;
                    }
                    else
                    {
                        selectedLevelsStrings.remove(levels[iter]);
                        isSelectedLevelArray[iter] = false;
                    }
                }
            });
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    databaseResultsFiltered();
                    dialogInterface.dismiss();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            builder.show();
        }
    };

    private void databaseResultsFiltered()
    {
        connector.openDB();

        if(!selectedLevelsStrings.isEmpty())
        {
            c = connector.getAllFiltered(selectedLevelsStrings);
        }
        else
        {
            c = connector.getAll();
        }
        databaseModelArrayList.clear();

        if(c.moveToFirst())
        {
            while(!c.isAfterLast())
            {
                DatabaseModel model = new DatabaseModel();
                model.setId(c.getLong(c.getColumnIndex("_id")));
                model.setName(c.getString(c.getColumnIndex("name")));
                model.setOnMove(c.getString(c.getColumnIndex("onmove")));
                model.setMoveNr(c.getInt(c.getColumnIndex("movenr")));
                model.setQuestion(c.getString(c.getColumnIndex("question")));
                model.setHint(c.getString(c.getColumnIndex("hint")));
                model.setLevel(c.getString(c.getColumnIndex("level")));
                model.setAnswer(c.getString(c.getColumnIndex("answer")));
                model.setTimer(c.getString(c.getColumnIndex("timer")));
                model.setFen(c.getString(c.getColumnIndex("fen")));
                model.setPgn(c.getString(c.getColumnIndex("pgn")));

                databaseModelArrayList.add(model);
                c.moveToNext();
            }
        }
        adapter.notifyDataSetChanged();
        c.close();
    }
}
