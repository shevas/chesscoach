package com.hetman.chesscoach;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;


public class LoginActivity extends AppCompatActivity
{
    private EditText mUsernameView;
    private ProgressBar mBar;
    private TextView welcomeText;

    private SharedPreferences sharedPrefs;

    private LinearLayout ll1;

    CountDownTimer timer;
    int time = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Context context = getApplicationContext();
        sharedPrefs = context.getSharedPreferences(getString(R.string.preferences_key),Context.MODE_PRIVATE);

        ll1 = (LinearLayout) findViewById(R.id.email_login_form);
        mUsernameView = (EditText) findViewById(R.id.email);
        mBar = (ProgressBar) findViewById(R.id.progress_bar_login);
        mBar.setVisibility(View.INVISIBLE);

        timer = new CountDownTimer(2000,100) {
            @Override
            public void onTick(long l) {
                time += 5;
                mBar.setProgress(time);
            }

            @Override
            public void onFinish() {
                moveToMainScreen();
            }
        };

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mUsernameView.getText().toString().length() < 1)
                {
                    Toast t = Toast.makeText(getApplicationContext(),"Enter name/nickname first!",Toast.LENGTH_SHORT);
                    t.show();
                }
                else
                {
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString("remembered_username",mUsernameView.getText().toString());
                    editor.commit();
                    mBar.setVisibility(View.VISIBLE);
                    timer.start();
                }
            }
        });

        if(sharedPrefs.contains("remembered_username"))
        {
            String username = sharedPrefs.getString("remembered_username","");
            mUsernameView.setVisibility(View.INVISIBLE);
            mEmailSignInButton.setVisibility(View.INVISIBLE);
            ((ViewGroup) mUsernameView.getParent()).removeView(mUsernameView);
            ((ViewGroup) mEmailSignInButton.getParent()).removeView(mEmailSignInButton);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            welcomeText = (TextView) findViewById(R.id.welcome_text);
            welcomeText.setLayoutParams(params);
            welcomeText.setText("Welcome back, "+username+"!");

            mBar.setVisibility(View.VISIBLE);
            timer.start();
        }
    }

    private void moveToMainScreen()
    {
        Intent intent = new Intent(getApplicationContext(), MainScreen.class);
        finish();
        startActivity(intent);
    }
}

