package com.hetman.chesscoach.chessboard;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.hetman.chesscoach.R;
import com.hetman.chesscoach.values.ChessPieces;

/**
 * Created by michm on 10.03.2017.
 */

public final class PieceTile
{
    final Context context;
    private int squareSize;
    private int xcoord = 0;
    private int ycoord = 0;

    private Rect tileRect;

    private ChessPieces chesspiece;
    private boolean color;
    private int id;

    private String lineH;
    private String lineV;

    private boolean selected = false;

    // Constructor
    public PieceTile(Context context, ChessPieces chesspiece, boolean color, int id)
    {
        this.context = context;
        this.chesspiece = chesspiece;
        this.color = color;
        this.id = id;
    }

    public void setHorizontal(String h)
    {
        lineH = h;
    }

    public void setVertical(String v)
    {
        lineV = v;
    }

    public String getVertical()
    {
        return lineV;
    }

    public String getHorizontal()
    {
        return lineH;
    }

    public void setSelected(boolean s)
    {
        selected = s;
    }

    public boolean getSelected()
    {
        return selected;
    }

    public void setCoord(int xcoord, int ycoord)
    {
        this.xcoord = xcoord;
        this.ycoord = ycoord;

        final Rect tileRect = new Rect(
                xcoord,               // left
                ycoord,               // top
                xcoord + squareSize,  // right
                ycoord + squareSize   // bottom
        );

        this.tileRect = tileRect;
    }

    public int getXcoord()
    {
        return xcoord;
    }

    public int getYcoord()
    {
        return ycoord;
    }

    public ChessPieces getChesspiece()
    {
        return chesspiece;
    }

    public void setColor(boolean color)
    {
        this.color = color;
    }

    public boolean getColor()
    {
        return color;
    }

    public void setSquareSize(int squareSize)
    {
        this.squareSize = squareSize;
    }

    public void draw(final Canvas canvas)
    {
        Resources res = context.getResources();
        Drawable piece_image;

        switch(chesspiece)
        {
            case PAWN:
            {
                if(color)
                {
                    piece_image = res.getDrawable(R.drawable.bialy_pion);

                }
                else
                {
                    piece_image = res.getDrawable(R.drawable.czarny_pion);

                }
                break;
            }

            case KNIGHT:
            {
                if(color)
                {
                    piece_image = res.getDrawable(R.drawable.bialy_kon);

                }
                else
                {
                    piece_image = res.getDrawable(R.drawable.czarny_kon);

                }
                break;
            }

            case BISHOP:
            {
                if(color)
                {
                    piece_image = res.getDrawable(R.drawable.bialy_goniec);

                }
                else
                {
                    piece_image = res.getDrawable(R.drawable.czarny_goniec);

                }
                break;
            }

            case ROOK:
            {
                if(color)
                {
                    piece_image = res.getDrawable(R.drawable.biala_wieza);

                }
                else
                {
                    piece_image = res.getDrawable(R.drawable.czarna_wieza);

                }
                break;
            }

            case QUEEN:
            {
                if(color)
                {
                    piece_image = res.getDrawable(R.drawable.bialy_hetman);

                }
                else
                {
                    piece_image = res.getDrawable(R.drawable.czarny_hetman);

                }
                break;
            }

            case KING:
            {
                if(color)
                {
                    piece_image = res.getDrawable(R.drawable.bialy_krol);

                }
                else
                {
                    piece_image = res.getDrawable(R.drawable.czarny_krol);

                }
                break;
            }

            default: throw new AssertionError();
        }
        piece_image.setBounds(xcoord,ycoord,xcoord+squareSize,ycoord+squareSize);
        piece_image.draw(canvas);

        if(selected && tileRect != null)
        {
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(8);
            paint.setColor(ContextCompat.getColor(context,R.color.simpleWhite));
            canvas.drawRect(tileRect,paint);
        }
    }

    public boolean isTouched(int x, int y)
    {
        return tileRect.contains(x, y);
    }
}
