package com.hetman.chesscoach.chessboard;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.hetman.chesscoach.chessboard.viewmodels.ChessboardModel;
import com.hetman.chesscoach.tools.ViewToBoardTools;
import com.hetman.chesscoach.values.ChessPieces;
import com.hetman.chesscoach.values.pieces.ChessPiece;

/**
 * Created by shevson on 4/16/17.
 */

public class ChessboardGame extends ChessboardModel
{
    private boolean firstDrawDone = false;

    // Event listener - callbacks
    public interface PieceToMoveEventListener {
        void onEventOccured(ChessPiece cp, String xfrom, String yfrom, String xto, String yto);
    }

    public interface DidDrawListener {
        boolean onDrawEnded();
    }

    private PieceToMoveEventListener myListener;
    private DidDrawListener drawListener;

    private PieceTile pieceToMove = null;
    private PieceTile lastMoved = null;

    private PieceTile pieceToPromote = null;

    // Event listener when a piece is moved
    public void setEventListener(PieceToMoveEventListener myListener)
    {
        this.myListener = myListener;
    }

    public void setDrawListener(DidDrawListener drawListener)
    {
        this.drawListener = drawListener;
    }

    // Constructor
    public ChessboardGame(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
        this.mContext = context;
        this.mTiles = new Tile[COLS][ROWS];

        setFocusable(true);
        ids = 0;

        buildTiles(context);
    }

    @Override
    protected void onDraw(final Canvas canvas)
    {
        final int width = getWidth();
        final int height = getHeight();

        this.squareSize = Math.min(
                getSquareSizeWidth(width),
                getSquareSizeHeight(height)
        );

        this.mCanvas = canvas;
        computeOrigins();

        String row = "";
        String column = "";
        int rownr;
        int colnr;

        // Main Chess Board

        for (int c = 0; c < COLS; c++)
        {
            for (int r = 0; r < ROWS; r++)
            {
                final int xCoord = getXCoord(c);
                final int yCoord = getYCoord(r);

                if(flipped)
                {
                    rownr = 7-r;
                    colnr = 7-c;
                }
                else
                {
                    rownr = r;
                    colnr = c;
                }

                final Rect tileRect = new Rect(
                        xCoord,               // left
                        yCoord,               // top
                        xCoord + squareSize,  // right
                        yCoord + squareSize   // bottom
                );

                mTiles[c][r].setTileRect(tileRect);
                mTiles[c][r].setCoords(xCoord, yCoord);
                mTiles[c][r].setHorizontal(rownr);
                mTiles[c][r].setVertical(colnr);
                mTiles[c][r].draw(canvas);


                // Annotations

                column = mTiles[c][r].getColumnString();
                row = String.valueOf(rownr+1);

                if(c == 0)
                {
                    Paint paint = new Paint();
                    paint.setColor(Color.BLACK);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setTextSize(15 * getResources().getDisplayMetrics().density);
                    paint.setTextAlign(Paint.Align.CENTER);
                    canvas.drawText(row,xCoord-(squareSize/4),yCoord+(squareSize/2), paint);
                }

                if(r == 0)
                {
                    Paint paint = new Paint();
                    paint.setColor(Color.BLACK);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setTextSize(15 * getResources().getDisplayMetrics().density);
                    canvas.drawText(column,xCoord+(squareSize/2),yCoord+(squareSize)+(squareSize/2), paint);
                }
            }
        }

        // Pieces added to position
        if(addedPieces.size()!=0)
        {
            for(PieceTile piece : addedPieces)
            {
                for(int c=0; c<COLS; c++)
                {
                    for(int r=0; r<ROWS; r++)
                    {
                        if(piece.getHorizontal().equals(mTiles[c][r].getRowString()) && piece.getVertical().equals(mTiles[c][r].getColumnString()))
                        {
                            piece.setSquareSize(squareSize);
                            piece.setCoord(mTiles[c][r].getXcoord(), mTiles[c][r].getYcoord());
                            piece.draw(canvas);
                        }
                    }
                }
            }
        }
        if(!firstDrawDone)
        {
            drawListener.onDrawEnded();
            firstDrawDone = true;
        }
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event)
    {
        if(event.getAction()==MotionEvent.ACTION_UP)
        {
            boolean piece_touched = false;
            final int x = (int) event.getX();
            final int y = (int) event.getY();

            Tile tile;

            for (PieceTile pt : addedPieces)
            {
                if (pt.isTouched(x, y))
                {
                    if (pieceToMove != null)
                    {
                        if (pt.equals(pieceToMove))
                        {
                            pt.setSelected(false);
                            pieceToMove = null;
                        }
                        else
                        {
                            // Try to take this piece
                            myListener.onEventOccured(ViewToBoardTools.typeToPiece(pieceToMove.getChesspiece(),pieceToMove.getColor()), pieceToMove.getHorizontal(), pieceToMove.getVertical(), pt.getHorizontal(), pt.getVertical());
                            pieceToMove.setSelected(false);
                            pieceToMove = null;
                        }
                    }
                    else
                    {
                        pt.setSelected(true);
                        pieceToMove = pt;
                    }
                    piece_touched = true;
                }
            }

            if(!piece_touched)
            {
                for (int c = 0; c < COLS; c++)
                {
                    for (int r = 0; r < ROWS; r++)
                    {
                        tile = mTiles[c][r];
                        if (tile.isTouched(x, y))
                        {
                            if(pieceToMove != null)
                            {
                                // Try to move the piece
                                myListener.onEventOccured(ViewToBoardTools.typeToPiece(pieceToMove.getChesspiece(),pieceToMove.getColor()), pieceToMove.getHorizontal(), pieceToMove.getVertical(), tile.getRowString(), tile.getColumnString());
                                pieceToMove.setSelected(false);
                                pieceToMove = null;
                            }
                        }
                    }
                }
            }
            this.invalidate();
        }
        return true;
    }
}
