package com.hetman.chesscoach.chessboard;

import com.hetman.chesscoach.tools.ChessboardLogicHelper;
import com.hetman.chesscoach.tools.ViewToBoardTools;
import com.hetman.chesscoach.values.ChessPieces;
import com.hetman.chesscoach.values.pieces.Bishop;
import com.hetman.chesscoach.values.pieces.ChessPiece;
import com.hetman.chesscoach.values.pieces.King;
import com.hetman.chesscoach.values.pieces.Knight;
import com.hetman.chesscoach.values.pieces.Pawn;
import com.hetman.chesscoach.values.pieces.Queen;
import com.hetman.chesscoach.values.pieces.Rook;

/**
 * Created by shevson on 4/15/17.
 */

public class ChessboardController implements Cloneable
{
    private ChessPiece[][] boardState;
    private int boardsize = 8;

    // Check
    private boolean isChecked = false;
    private King checkedKing = null;
    private boolean stillChecked2 = false;

    // Mate and On Move
    private boolean isMate = false;
    private boolean onMoveset = false;
    private boolean onMove;

    // Enpassant
    private boolean possibleEnpassant = false;
    private boolean didEnpassant = false;
    private int enpd = 0;

    // Promotion
    private int xpromo = -1;
    private int ypromo = -1;
    private boolean promo_colour;
    private boolean isPromoting;

    // Castle
    private boolean wkingsideAvailable = false;
    private boolean wqueensideAvailable = false;
    private boolean bkingsideAvailable = false;
    private boolean bqueensideAvailable = false;
    private boolean kingCastled = false;

    // Moving vs Taking
    private boolean isTaking = false;

    // Same piece coordinate
    private String coordinate = "";

    // Initial Position
    private ChessPiece [] initialPosition1 = {new Rook(true), new Knight(true), new Bishop(true), new Queen(true), new King(true), new Bishop(true), new Knight(true), new Rook(true)};
    private ChessPiece [] initialPosition2 = new ChessPiece[8];
    private ChessPiece [] initialPosition8 = {new Rook(false), new Knight(false), new Bishop(false), new Queen(false), new King(false), new Bishop(false), new Knight(false), new Rook(false)};
    private ChessPiece [] initialPosition7 = new ChessPiece[8];

    public ChessboardController()
    {
        boardState = new ChessPiece[boardsize][boardsize];
        for(int k=0; k<initialPosition2.length; k++)
        {
            initialPosition2[k] = new Pawn(true);
            initialPosition7[k] = new Pawn(false);
        }
    }

    public ChessboardController(ChessPiece [][] pieces)
    {
        boardState = pieces;

        // Checking if castle available
        if(boardState[0][4]!=null && boardState[7][4]!=null)
        {
            if(boardState[0][4].getType()==ChessPieces.KING && boardState[0][4].getColor())
            {
                if(boardState[0][0]!=null)
                {
                    if(boardState[0][0].getType()==ChessPieces.ROOK && boardState[0][0].getColor())
                    {
                        wqueensideAvailable = true;
                    }

                }
                if(boardState[0][7]!=null)
                {
                    if(boardState[0][7].getType()==ChessPieces.ROOK && boardState[0][7].getColor())
                    {
                        wkingsideAvailable = true;
                    }
                }
            }

            if(boardState[7][4].getType()==ChessPieces.KING && !boardState[7][4].getColor())
            {
                if(boardState[7][0]!=null)
                {
                    if(boardState[7][0].getType()==ChessPieces.ROOK && !boardState[7][0].getColor())
                    {
                        bqueensideAvailable = true;
                    }

                }
                if(boardState[7][7]!=null)
                {
                    if(boardState[7][7].getType()==ChessPieces.ROOK && !boardState[7][7].getColor())
                    {
                        bkingsideAvailable = true;
                    }
                }
            }
        }

        // Check for checks
        this.isChecked = check4Checks(true, true);
        if(isChecked)
        {
            onMove = true;
            onMoveset = true;
        }
        else
        {
            this.isChecked = check4Checks(false, true);
            if(isChecked)
            {
                onMove = false;
                onMoveset = true;
            }
        }

        if(isChecked)
        {
            check4Mates(onMove);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ChessboardController cc = new ChessboardController();
        cc = (ChessboardController) super.clone();
        ChessPiece[][] position = new ChessPiece[8][8];
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                position[i][j] = this.boardState[i][j];
            }
        }
        cc.boardState = position;
        return cc;
    }

    // Set initial chess position
    public void setInitialPosition()
    {
        for(int x=0; x<boardsize; x++)
        {
            for(int y=0; y<boardsize; y++)
            {
                boardState[x][y] = null;
            }
        }

        for(int k=0; k<boardsize; k++)
        {
            boardState[0][k] = initialPosition1[k];
            boardState[1][k] = initialPosition2[k];
            boardState[6][k] = initialPosition7[k];
            boardState[7][k] = initialPosition8[k];
        }
        wkingsideAvailable = true;
        wqueensideAvailable = true;
        bkingsideAvailable = true;
        bqueensideAvailable = true;
    }

    // Get/Set square
    public ChessPiece getSquare(int x, int y)
    {
        return boardState[x][y];
    }

    public void setSquare(int x, int y, ChessPiece piece)
    {
        boardState[x][y] = piece;
    }

    // Set/Get whose on move
    public void setOnMove(boolean onMove)
    {
        this.onMoveset = true;
        this.onMove = onMove;
    }

    public boolean getOnMove()
    {
        return this.onMove;
    }

    public boolean isOnMoveSet()
    {
        return this.onMoveset;
    }

    // Get isTaking/isChecking/isMating

    public boolean isTaking()
    {
        return isTaking;
    }

    public boolean isChecking()
    {
        return isChecked;
    }

    public boolean wasCastled()
    {
        boolean castled = kingCastled;
        kingCastled = false;
        return castled;
    }

    public boolean wasMated()
    {
        return isMate;
    }

    // Same piece attacks
    public String getSameCoordinate()
    {
        return this.coordinate;
    }

    // En Passant methods

    public void setEnPassant(boolean enPassant)
    {
        possibleEnpassant = enPassant;
    }

    public void didEnPassant(int enpd)
    {
        didEnpassant = true;
        this.enpd = enpd;
    }

    private void cleanEnPassantFlags()
    {
        for(int k=0; k<boardsize; k++)
        {
            for(int j=0; j<boardsize; j++)
            {
                if(boardState[k][j]!=null)
                {
                    if(boardState[k][j] instanceof Pawn)
                    {
                        Pawn p = (Pawn) boardState[k][j];
                        p.cleanEnPassantFlag();
                    }
                }
            }
        }
    }

    // Promoting pawn methods

    public void isPawnPromoting(int x, int y, boolean colour)
    {
        xpromo = x;
        ypromo = y;
        promo_colour = colour;
        isPromoting = true;
    }

    public void promote(ChessPieces piece)
    {
        ChessPiece cp = ViewToBoardTools.typeToPiece(piece, promo_colour);
        boardState[xpromo][ypromo] = cp;
        isPromoting = false;
        xpromo = -1;
        ypromo = -1;
        cp.attackedByMe(xpromo, ypromo,this);
        isChecked = check4Checks(onMove, false);
        check4Mates(onMove);
    }

    public boolean isPromoting()
    {
        return isPromoting;
    }

    public boolean getPromoColour()
    {
        return promo_colour;
    }

    // Castling methods

    public boolean  isCastleKingSideAvailable(boolean color)
    {
        if(color)
        {
            return wkingsideAvailable;
        }
        else
        {
            return bkingsideAvailable;
        }
    }

    public boolean isCastleQueenSideAvailable(boolean color)
    {
        if(color)
        {
            return wqueensideAvailable;
        }
        else
        {
            return bqueensideAvailable;
        }
    }

    public void setCastleKingSideAvailable(boolean toset, boolean color)
    {
        if(color)
        {
            wkingsideAvailable = toset;
        }
        else
        {
            bkingsideAvailable = toset;
        }
    }

    public void setCastleQueenSideAvailable(boolean toset, boolean color)
    {
        if(color)
        {
            wqueensideAvailable = toset;
        }
        else
        {
            bqueensideAvailable = toset;
        }
    }

    public void kingWasCastled(boolean color, boolean side)
    {
        ChessPiece rook;
        if(side)
        {
            if(color)
            {
                rook = boardState[0][7];
                boardState[0][7] = null;
                boardState[0][5] = rook;
            }
            else
            {
                rook = boardState[7][7];
                boardState[7][7] = null;
                boardState[7][5] = rook;
            }
        }
        else
        {
            if(color)
            {
                rook = boardState[0][0];
                boardState[0][0] = null;
                boardState[0][3] = rook;
            }
            else
            {
                rook = boardState[7][0];
                boardState[7][0] = null;
                boardState[7][3] = rook;
            }
        }
        kingCastled = true;
    }

    // Checking for checks
    private boolean check4Checks(boolean checkingColor, boolean virtualCheck)
    {
        boolean isChecked = false;
        // LOG
        int kingx = -1;
        int kingy = -1;
        for(int a=0; a<8; a++)
        {
            for(int b=0; b<8; b++)
            {
                if(boardState[a][b] instanceof King)
                {
                    if(boardState[a][b].getColor()==checkingColor)
                    {
                        kingx = a;
                        kingy = b;
                    }
                }
            }
        }
        boolean [][] attackedSquares;

        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(boardState[i][j]!=null)
                {
                    if(boardState[i][j].getColor()!=checkingColor)
                    {
                        attackedSquares = boardState[i][j].attackedByMe(i,j,this);
                        for(int z=0; z<8; z++)
                        {
                            for(int x=0; x<8; x++)
                            {
                                if(attackedSquares[z][x]==true && z==kingx && x==kingy)
                                {
                                    isChecked = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return isChecked;
    }

    private void check4Mates(boolean onMove)
    {
        boolean [][] attacked;
        boolean isMate = true;
        ChessPiece cp;
        ChessPiece virtuallyTakenPiece = null;
        if(isChecked)
        {
            for(int i=0; i<8; i++)
            {
                for(int j=0; j<8; j++)
                {
                    if(boardState[i][j]!=null)
                    {
                        if(boardState[i][j].getColor()==onMove)
                        {
                            if(boardState[i][j].getType()==ChessPieces.PAWN)
                            {
                                int nxcoord = -1;
                                if(boardState[i][j].tryToMove(this, i, j, i+1, j, true))
                                {
                                    nxcoord = i+1;
                                }
                                else if(boardState[i][j].tryToMove(this, i, j, i+2, j, true))
                                {
                                    nxcoord = i+2;
                                }
                                else if(boardState[i][j].tryToMove(this, i, j, i-1, j, true))
                                {
                                    nxcoord = i-1;
                                }
                                else if(boardState[i][j].tryToMove(this, i, j, i-2, j, true))
                                {
                                    nxcoord = i-2;
                                }

                                if(nxcoord != -1)
                                {
                                    cp = boardState[i][j];
                                    boardState[nxcoord][j] = cp;
                                    boardState[i][j] = null;
                                    if(check4Checks(onMove, true))
                                    {
                                        boardState[i][j] = cp;
                                        boardState[nxcoord][j] = null;
                                    }
                                    else
                                    {
                                        boardState[i][j] = cp;
                                        boardState[nxcoord][j] = null;
                                        isMate = false;
                                    }
                                }
                            }
                            if(isMate)
                            {
                                attacked = boardState[i][j].attackedByMe(i,j,this);
                                for(int a=0; a<8; a++)
                                {
                                    for(int b=0; b<8; b++)
                                    {
                                        if(attacked[a][b]==true)
                                        {
                                            if(boardState[i][j].tryToMove(this, i, j, a, b, true))
                                            {
                                                cp = boardState[i][j];
                                                if(boardState[a][b]!=null)
                                                {
                                                    virtuallyTakenPiece = boardState[a][b];
                                                }
                                                boardState[a][b] = cp;
                                                boardState[i][j] = null;
                                                // LOG
                                                ChessboardLogicHelper.printChessboard(boardState);
                                                if(check4Checks(onMove, true))
                                                {
                                                    // Still under check
                                                }
                                                else
                                                {
                                                    isMate = false;
                                                }
                                                boardState[i][j] = cp;
                                                if(virtuallyTakenPiece!=null)
                                                {
                                                    boardState[a][b] = virtuallyTakenPiece;
                                                    virtuallyTakenPiece = null;
                                                }
                                                else
                                                {
                                                    boardState[a][b] = null;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.isMate = isMate;
            if(this.isMate)
            {
                // Mate!
            }
        }
    }

    // Check if another piece can move to the same square
    private void checkForOtherPiece(ChessPiece cp, int xfrom, int yfrom, int xto, int yto)
    {
        boolean [][] attacked;
        String basecoordinate = "";
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(boardState[i][j]!=null)
                {
                    if(boardState[i][j].getType()==cp.getType() && boardState[i][j].getColor()==cp.getColor())
                    {
                        if(xfrom!=i || yfrom!=j)
                        {
                            attacked = boardState[i][j].attackedByMe(i,j,this);
                            if(attacked[xto][yto])
                            {
                                if(yfrom==j)
                                {
                                    basecoordinate = String.valueOf(xfrom+1);
                                }
                                else
                                {
                                    basecoordinate = ViewToBoardTools.coordToString(yfrom);
                                }
                            }
                        }
                    }
                }
            }
        }

        coordinate = basecoordinate;
    }

    // Position verification
    public boolean verifyPosition(ChessPiece [][] position)
    {
        int white_king_counter = 0;
        int black_king_counter = 0;
        int white_pawn_counter = 0;
        int black_pawn_counter = 0;
        boolean wrong_pawn_setup = false;
        boolean kings_adjacent = false;

        King whiteking = null;
        King blackking = null;
        int wkx = -1;
        int wky = -1;
        int bkx = -1;
        int bky = -1;

        for(int x=0; x<boardsize; x++)
        {
            for(int y=0; y<boardsize; y++)
            {
                if(position[x][y] instanceof King)
                {
                    if(position[x][y].getColor())
                    {
                        whiteking = (King) position[x][y];
                        white_king_counter++;
                        wkx = x;
                        wky = y;
                    }
                    else
                    {
                        blackking = (King) position[x][y];
                        black_king_counter++;
                        bkx = x;
                        bky = y;
                    }
                }
                else if(position[x][y] instanceof Pawn)
                {
                    if(x == 0 || x == 7)
                    {
                        wrong_pawn_setup = true;
                    }
                    if(position[x][y].getColor())
                    {
                        white_pawn_counter++;
                    }
                    else
                    {
                        black_pawn_counter++;
                    }
                }
            }
        }

        if(white_king_counter==1 && black_king_counter==1 && whiteking!=null && blackking!=null)
        {
            boolean [][] whiteattacks = whiteking.attackedByMe(wkx, wky, this);
            for(int x=0; x<8; x++)
            {
                for(int y=0; y<8; y++)
                {
                    if(whiteattacks[x][y] && x == bkx && y == bky)
                    {
                        kings_adjacent = true;
                    }
                }
            }
        }

        if(white_king_counter!=1 || black_king_counter!=1 || white_pawn_counter>8 || black_pawn_counter>8 || kings_adjacent || wrong_pawn_setup)
        {
            // Wrong setup!
            return false;
        }
        else
        {
            return true;
        }
    }

    // Try to make a move!
    public boolean makeMove(ChessPiece cp, int xfrom, int yfrom, int xto, int yto)
    {
        ChessPiece takenPiece = null;
        // LOG
        ChessboardLogicHelper.printChessboard(boardState);
        boolean moveValid = false;
        if(onMove == cp.getColor() && !isMate)
        {
            if(boardState[xfrom][yfrom].getType().equals(cp.getType()) && boardState[xfrom][yfrom].getColor()==cp.getColor())
            {
                if(cp.tryToMove(this,xfrom, yfrom, xto, yto, false))
                {
                    isTaking = false;

                    // Check for taking
                    if(boardState[xto][yto]!=null)
                    {
                        takenPiece = boardState[xto][yto];
                        isTaking = true;
                    }

                    // Check for en passant
                    if(didEnpassant)
                    {
                        takenPiece = boardState[xfrom][yfrom+enpd];
                        boardState[xfrom][yfrom+enpd] = null;
                        isTaking = true;
                    }

                    moveValid = true;

                    // Check if other piece of the same type and color can move to this location (needed for annotation)
                    if(!cp.getType().equals(ChessPieces.PAWN))
                    {
                        checkForOtherPiece(cp, xfrom, yfrom, xto, yto);
                    }
                    else
                    {
                        coordinate = "";
                    }
                    boardState[xfrom][yfrom] = null;
                    boardState[xto][yto] = cp;

                    // Check if the king is not under a check
                    if(check4Checks(onMove, false))
                    {
                        moveValid = false;
                        boardState[xfrom][yfrom] = cp;
                        if(takenPiece!=null)
                        {
                            if(didEnpassant)
                            {
                                boardState[xfrom][yfrom+enpd] = takenPiece;
                                boardState[xto][yto] = null;
                                didEnpassant = false;
                            }
                            else
                            {
                                boardState[xto][yto] = takenPiece;
                            }
                        }
                        else
                        {
                            boardState[xto][yto] = null;
                        }
                    }

                    if(moveValid)
                    {
                        if(didEnpassant)
                        {
                            boardState[xfrom][yfrom+enpd] = null;
                            didEnpassant = false;
                        }

                        // Clear en passant flags
                        if(!possibleEnpassant)
                        {
                            cleanEnPassantFlags();
                        }
                        else
                        {
                            possibleEnpassant = false;
                        }

                        // Change on move
                        if(onMove)
                        {
                            onMove = false;
                        }
                        else
                        {
                            onMove = true;
                        }

                        // Check if there's check after the move
                        isChecked = check4Checks(onMove, false);

                        // Check if it's mate after the move
                        check4Mates(onMove);
                    }
                }
            }
        }
        return moveValid;
    }

    // Make move from notation
    public boolean makeMove(ChessPiece cp, int xto, int yto, char extrafrom, boolean color)
    {
        boolean toReturn = false;

        int extraX = -1;
        int extraY = -1;

        if(extrafrom!='-')
        {
            if(Character.isDigit(extrafrom))
            {
                extraX = Integer.valueOf(String.valueOf(extrafrom))-1;
            }
            else
            {
                extraY = ViewToBoardTools.stringToCoord(String.valueOf(extrafrom));
            }
        }

        onMove = color;
        onMoveset = true;

        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(boardState[i][j]!=null)
                {
                    if(boardState[i][j].getType() == cp.getType() && boardState[i][j].getColor()==color)
                    {
                        if(extraX != -1)
                        {
                            if(i == extraX)
                            {
                                if(makeMove(cp, i,j,xto,yto))
                                {
                                    toReturn = true;
                                    break;
                                }
                            }
                        }
                        else if(extraY != -1)
                        {
                            if(j == extraY)
                            {
                                if(makeMove(cp, i,j,xto,yto))
                                {
                                    toReturn = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if(makeMove(cp, i,j,xto,yto))
                            {
                                toReturn = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return toReturn;
    }
}
