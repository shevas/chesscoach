package com.hetman.chesscoach.chessboard.viewmodels;

import android.view.View;

/**
 * Created by shevson on 4/15/17.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.hetman.chesscoach.chessboard.PieceTile;
import com.hetman.chesscoach.chessboard.Tile;
import com.hetman.chesscoach.tools.ViewToBoardTools;
import com.hetman.chesscoach.values.ChessPieces;
import com.hetman.chesscoach.values.pieces.ChessPiece;

import java.util.ArrayList;

public abstract class ChessboardModel extends View
{

    protected static int COLS = 8;
    protected static int ROWS = 8;

    protected Tile[][] mTiles;
    protected ArrayList<PieceTile> addedPieces = new ArrayList<>();

    protected Context mContext;
    protected Canvas mCanvas;
    protected int ids;

    protected int x0 = 0;
    protected int y0 = 0;
    protected int squareSize = 0;
    protected boolean flipped = false;

    // Constructor
    public ChessboardModel(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
        this.mContext = context;
        this.mTiles = new Tile[COLS][ROWS];

        setFocusable(true);
        ids = 0;

        buildTiles(context);
    }

    // Build only chessboard tiles
    protected void buildTiles(Context context)
    {
        for (int c = 0; c < COLS; c++)
        {
            for (int r = 0; r < ROWS; r++)
            {
                mTiles[c][r] = new Tile(c, r, context);
            }
        }
    }

    @Override
    protected void onDraw(final Canvas canvas)
    {
        System.out.print("Drawing Chessboard");
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event)
    {
        if(event.getAction()==MotionEvent.ACTION_UP)
        {
            final int x = (int) event.getX();
            final int y = (int) event.getY();

            Tile tile;
            for (int c = 0; c < COLS; c++)
            {
                for (int r = 0; r < ROWS; r++)
                {
                    tile = mTiles[c][r];
                    if (tile.isTouched(x, y))
                    {
                        System.out.println("Tile "+x+" "+y+" was touched");
                    }
                }
            }
        }

        return true;
    }

    // Return the size of the square (width)
    protected int getSquareSizeWidth(final int width)
    {
        int squarewidth = width / 9;
        return squarewidth;
    }

    // Return the size of the square (height)
    protected int getSquareSizeHeight(final int height)
    {
        int squareheight = height / 9;
        return squareheight;
    }

    // Return starting x coordinate
    protected int getXCoord(final int x)
    {
        return x0 + squareSize * x;
    }

    // Return starting y coordinate
    protected int getYCoord(final int y)
    {
        return y0 + squareSize * (7 - y);
    }

    // Compute chessboard origins
    protected void computeOrigins()
    {
        this.x0 = 0 + (squareSize/2);
        this.y0 = 0;
    }

    // Flip the board
    public void setFlipped(boolean flipped)
    {
        this.flipped = flipped;
        redraw();
    }

    // Check if board is flipped
    public Boolean getFlipped()
    {
        return this.flipped;
    }

    // Set new piece on board
    public void setPiece (ChessPieces cp, boolean color, String h, String v)
    {
        PieceTile pt = new PieceTile(mContext,cp, color, ids++);
        pt.setSquareSize(squareSize);
        pt.setHorizontal(h);
        pt.setVertical(v);
        addedPieces.add(pt);
    }

    // Return all pieces currently on board
    public ChessPiece [][] getPieces()
    {
        ChessPiece [][] pieces = new ChessPiece[8][8];
        for(int i=0; i<addedPieces.size(); i++)
        {
            ChessPieces type = addedPieces.get(i).getChesspiece();
            boolean color = addedPieces.get(i).getColor();
            ChessPiece cp = ViewToBoardTools.typeToPiece(type,color);
            pieces[Integer.parseInt(addedPieces.get(i).getHorizontal())][ViewToBoardTools.stringToCoord(addedPieces.get(i).getVertical())] = cp;
        }
        return pieces;
    }

    // Set pieces on board
    public void setPieces (ChessPiece [][] pieces)
    {
        ArrayList<PieceTile> newposition = new ArrayList<PieceTile>();
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(pieces[i][j]!=null)
                {
                    PieceTile pt = new PieceTile(mContext, pieces[i][j].getType(), pieces[i][j].getColor(),ids++);
                    pt.setSquareSize(squareSize);
                    pt.setHorizontal(String.valueOf(i));
                    pt.setVertical(ViewToBoardTools.coordToString(j));
                    newposition.add(pt);
                }
            }
        }
        addedPieces = newposition;
        redraw();
    }

    // Redraw chessboard
    public void redraw()
    {
        this.invalidate();
    }
}
