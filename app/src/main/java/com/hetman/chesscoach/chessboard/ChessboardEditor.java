package com.hetman.chesscoach.chessboard;

/**
 * Created by pi12366 on 10.03.2017.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.hetman.chesscoach.R;
import com.hetman.chesscoach.chessboard.viewmodels.ChessboardModel;
import com.hetman.chesscoach.values.ChessPieces;

import java.util.ArrayList;

public class ChessboardEditor extends ChessboardModel {
    private static final String TAG = ChessboardEditor.class.getSimpleName();

    private static final int PIECES = 12;

    private final PieceTile [] mEditorPieces;
    private PieceTile newpiece = null;
    private PieceTile piece_to_add = null;

    private boolean pieceTouched = false;

    private boolean firstDrawDone = false;

    public interface DidDrawListener {
        boolean onDrawEnded();
    }
    private ChessboardGame.DidDrawListener drawListener;

    // Check if board was drawn (onCreate)
    public void setDrawListener(ChessboardGame.DidDrawListener drawListener)
    {
        this.drawListener = drawListener;
    }

    // Constructor
    public ChessboardEditor(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
        this.mContext = context;
        this.mTiles = new Tile[COLS][ROWS];
        this.mEditorPieces = new PieceTile[PIECES];

        setFocusable(true);
        ids = 0;

        buildTiles(context);
        buildPieceTiles(context);
    }

    // Extra piece tiles (for editor)
    private void buildPieceTiles(Context context)
    {
        int iter = 0;

        for(int c = 0; c < PIECES; c++)
        {
            if(iter==(PIECES/2))
            {
                iter = 0;
            }

            if(c<=((PIECES/2)-1))
            {
                mEditorPieces[c] = new PieceTile(context, ChessPieces.values()[iter], true, ids++);
            }
            else
            {
                mEditorPieces[c] = new PieceTile(context, ChessPieces.values()[iter], false, ids++);
            }
            iter++;
        }
    }

    @Override
    protected int getSquareSizeWidth(final int width)
    {
        int squarewidth = width / 9;
        return squarewidth;
    }

    @Override
    protected int getSquareSizeHeight(final int height)
    {
        int squareheight = height / 11;
        return squareheight;
    }

    @Override
    protected void onDraw(final Canvas canvas)
    {
        final int width = getWidth();
        final int height = getHeight();

        this.squareSize = Math.min(
                getSquareSizeWidth(width),
                getSquareSizeHeight(height)
        );

        this.mCanvas = canvas;
        computeOrigins();

        String row = "";
        String column = "";
        int rownr;
        int colnr;

        // Main Chess Board

        for (int c = 0; c < COLS; c++)
        {
            for (int r = 0; r < ROWS; r++)
            {
                final int xCoord = getXCoord(c);
                final int yCoord = getYCoord(r);

                if(flipped)
                {
                    rownr = 7-r;
                    colnr = 7-c;
                }
                else
                {
                    rownr = r;
                    colnr = c;
                }

                final Rect tileRect = new Rect(
                        xCoord,               // left
                        yCoord,               // top
                        xCoord + squareSize,  // right
                        yCoord + squareSize   // bottom
                );

                mTiles[c][r].setTileRect(tileRect);
                mTiles[c][r].setCoords(xCoord, yCoord);
                mTiles[c][r].setHorizontal(rownr);
                mTiles[c][r].setVertical(colnr);
                mTiles[c][r].draw(canvas);


                // Annotations

                column = mTiles[c][r].getColumnString();
                row = String.valueOf(rownr+1);

                if(c == 0)
                {
                    Paint paint = new Paint();
                    paint.setColor(Color.BLACK);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setTextSize(15 * getResources().getDisplayMetrics().density);
                    paint.setTextAlign(Paint.Align.CENTER);
                    canvas.drawText(row,xCoord-(squareSize/4),yCoord+(squareSize/2), paint);
                }

                if(r == 0)
                {
                    Paint paint = new Paint();
                    paint.setColor(Color.BLACK);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setTextSize(15 * getResources().getDisplayMetrics().density);
                    canvas.drawText(column,xCoord+(squareSize/2),yCoord+(squareSize)+(squareSize/2), paint);
                }
            }
        }

        int finxCoord = 0 + squareSize + squareSize/2;
        int finyCoord = 9 * squareSize;

        int basex = finxCoord;

        // Additional Pieces for Editor
        for(int c = 0; c < PIECES; c++)
        {
            mEditorPieces[c].setSquareSize(squareSize);
            mEditorPieces[c].setCoord(finxCoord, finyCoord);
            mEditorPieces[c].draw(canvas);

            if(c==((PIECES/2)-1))
            {
                finyCoord += squareSize;
                finxCoord = basex;
            }
            else
            {
                finxCoord += squareSize;
            }
        }

        // Pieces added to position
        if(addedPieces.size()!=0)
        {
            for(PieceTile piece : addedPieces)
            {
                for(int c=0; c<COLS; c++)
                {
                    for(int r=0; r<ROWS; r++)
                    {
                        if(piece.getHorizontal().equals(mTiles[c][r].getRowString()) && piece.getVertical().equals(mTiles[c][r].getColumnString()))
                        {
                            piece.setCoord(mTiles[c][r].getXcoord(), mTiles[c][r].getYcoord());
                            piece.draw(canvas);
                        }
                    }
                }
            }
        }

        // If new piece has to be added
        if(pieceTouched && piece_to_add!=null)
        {
            newpiece.draw(canvas);
            addedPieces.add(piece_to_add);
            piece_to_add = null;
        }

        if(!firstDrawDone)
        {
            drawListener.onDrawEnded();
            firstDrawDone = true;
        }
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event)
    {
        if(event.getAction()==MotionEvent.ACTION_UP)
        {
            boolean remove = false;
            PieceTile pt_to_remove = null;
            final int x = (int) event.getX();
            final int y = (int) event.getY();


            Tile tile;
            for (int c = 0; c < COLS; c++)
            {
                for (int r = 0; r < ROWS; r++)
                {
                    tile = mTiles[c][r];
                    if (tile.isTouched(x, y))
                    {
                        if(tile.handleTouch(pieceTouched))
                        {
                            for(PieceTile pt : addedPieces)
                            {
                                if(pt.isTouched(x,y))
                                {
                                    if(pt.getChesspiece().equals(newpiece.getChesspiece()))
                                    {
                                        remove = true;
                                    }
                                    pt_to_remove = pt;
                                }
                            }
                            if(pt_to_remove!=null)
                            {
                                addedPieces.remove(pt_to_remove);
                                pt_to_remove = null;
                            }

                            if(!remove)
                            {
                                newpiece.setCoord(tile.getXcoord(),tile.getYcoord());
                                newpiece.setSquareSize(squareSize);

                                piece_to_add = new PieceTile(mContext, newpiece.getChesspiece(), newpiece.getColor(), ids++);
                                piece_to_add.setSquareSize(squareSize);
                                piece_to_add.setVertical(tile.getColumnString());
                                piece_to_add.setHorizontal(tile.getRowString());
                                piece_to_add.setCoord(tile.getXcoord(),tile.getYcoord());

                                if(!tile.getHaspiece())
                                {
                                    tile.setHaspiece(true);
                                }
                            }
                            this.invalidate();
                        }
                    }
                }
            }
            for (int c = 0; c < PIECES; c++)
            {
                if(mEditorPieces[c].isTouched(x,y))
                {
                    for(int k = 0; k < PIECES; k++)
                    {
                        mEditorPieces[k].setSelected(false);
                    }
                    mEditorPieces[c].setSelected(true);
                    newpiece = new PieceTile(mContext, mEditorPieces[c].getChesspiece(), mEditorPieces[c].getColor(), ids++);
                    pieceTouched = true;
                    this.invalidate();
                }
            }
        }

        return true;
    }

    // Public function to clear the chessboard
    public void clearChessBoard()
    {
        addedPieces = new ArrayList<>();
        this.invalidate();
    }
}
