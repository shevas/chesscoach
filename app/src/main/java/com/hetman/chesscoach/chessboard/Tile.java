package com.hetman.chesscoach.chessboard;

/**
 * Created by pi12366 on 10.03.2017.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.hetman.chesscoach.R;

public final class Tile {
    private static final String TAG = Tile.class.getSimpleName();

    private final int col;
    private final int row;

    private final Paint squareColor;
    private Rect tileRect;

    private int xcoord;
    private int ycoord;

    private int lineH;
    private String lineV;

    private boolean haspiece = false;

    public Tile(final int col, final int row, final Context context)
    {
        this.col = col;
        this.row = row;

        this.squareColor = new Paint();
        squareColor.setColor(isDark() ? ContextCompat.getColor(context,R.color.logoColor) : ContextCompat.getColor(context,R.color.mainBackground));
    }

    public void draw(final Canvas canvas) {
        canvas.drawRect(tileRect, squareColor);
    }

    public void setVertical(int col)
    {
        String line = "";
        switch (col)
        {
            case 0: line = "a"; break;
            case 1: line = "b"; break;
            case 2: line = "c"; break;
            case 3: line = "d"; break;
            case 4: line = "e"; break;
            case 5: line = "f"; break;
            case 6: line = "g"; break;
            case 7: line = "h"; break;
            default: break;
        }
        lineV = line;
    }

    public void setHorizontal(int row)
    {
        lineH = row;
    }

    public String getColumnString()
    {
        return lineV;
    }

    public String getRowString()
    {
        return String.valueOf(lineH);
    }

    public boolean handleTouch(boolean isTouched)
    {
        if(isTouched)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isDark() {
        return (col + row) % 2 == 0;
    }

    public boolean isTouched(final int x, final int y) {
        return tileRect.contains(x, y);
    }

    public void setTileRect(final Rect tileRect) {
        this.tileRect = tileRect;
    }

    public void setCoords(int xcoord, int ycoord)
    {
        this.xcoord = xcoord;
        this.ycoord = ycoord;
    }

    public int getXcoord()
    {
        return this.xcoord;
    }

    public int getYcoord()
    {
        return this.ycoord;
    }

    public String toString() {
        final String column = getColumnString();
        final String row    = getRowString();
        return "<Tile " + column + row + ">";
    }

    public void setHaspiece(boolean haspiece)
    {
        this.haspiece = haspiece;
    }

    public boolean getHaspiece()
    {
        return haspiece;
    }

}
