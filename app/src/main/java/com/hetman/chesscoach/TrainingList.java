package com.hetman.chesscoach;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.hetman.chesscoach.values.FileData;
import com.hetman.chesscoach.menus.TrainingItem;

import java.io.File;
import java.util.ArrayList;

public class TrainingList extends AppCompatActivity {

    private ListView lv;
    private SharedPreferences sharedPrefs;
    private String [] solved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.training_toolbar);
        setSupportActionBar(toolbar);

        sharedPrefs = getApplicationContext().getSharedPreferences(getString(R.string.preferences_key),Context.MODE_PRIVATE);
        if(!sharedPrefs.contains("solved_exercises"))
        {
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString("solved_exercises","");
            editor.commit();
            solved = null;
        }
        else
        {
            solved = sharedPrefs.getString("solved_exercises","").split("#");
        }

        populateFileList(getApplicationContext());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        solved = sharedPrefs.getString("solved_exercises","").split("#");
        populateFileList(getApplicationContext());
    }

    private void populateFileList(Context ctx)
    {
        ArrayList<FileData> data = new ArrayList<>();
        File path = ctx.getDir("training", Context.MODE_PRIVATE);

        File [] inPathFiles = path.listFiles();

        if(inPathFiles.length>0)
        {
            for(int i=0; i<inPathFiles.length; i++)
            {
                String filename = inPathFiles[i].getName().substring(0, inPathFiles[i].getName().lastIndexOf("."));
                data.add(new FileData(filename, inPathFiles[i]));
            }
            TrainingItem adapter = new TrainingItem(TrainingList.this,ctx, R.layout.list_item_training_list, data, solved);
            lv = (ListView) findViewById(R.id.training_list);
            lv.setAdapter(adapter);
        }
    }
}
