package com.hetman.chesscoach;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hetman.chesscoach.annotations.Annotations;
import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.chessboard.ChessboardGame;
import com.hetman.chesscoach.tools.PgnHelper;
import com.hetman.chesscoach.tools.ViewToBoardTools;
import com.hetman.chesscoach.values.ChessPieces;
import com.hetman.chesscoach.values.pieces.ChessPiece;
import com.hetman.chesscoach.values.pieces.Pawn;

import org.w3c.dom.Text;

import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChessSolver extends AppCompatActivity {

    Context mContext;
    ChessboardGame cbview;
    ChessboardController chessboardController;
    Annotations gameAnnotation;
    Dialog promoDialog;
    Button startButton;
    TextView gameText;
    TextView questionText;
    long minutes;
    long seconds;
    CountDownTimer solveTimer;

    String [] settingUp = {"Setting up position", "Setting up position.", "Setting up position..", "Setting up position..."};
    int settingUpCounter = 0;

    String pgn;
    String filename;

    // Promotion
    String promotionfromx = "";
    String promotionfromy = "";
    String promotiontox = "";
    String promotiontoy = "";

    // Handler
    Handler moveshandler;

    // Shred Prefs
    SharedPreferences sharedPreferences;

    // Interaction
    boolean playerCanPlay = false;
    int untilmove = 0;
    boolean currentlyOnMove = false;
    boolean stopLoop = false;
    boolean isVariation = false;
    ChessboardController moveup = null;

    // Exercise done
    boolean exerciseDone = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chess_solver);
        Toolbar toolbar = (Toolbar) findViewById(R.id.solve_toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.preferences_key),Context.MODE_PRIVATE);

        mContext = getApplicationContext();
        cbview = (ChessboardGame) findViewById(R.id.eb_chessboard_solver);
        cbview.setEventListener(moveEventListener);
        cbview.setDrawListener(drawEndedListener);

        Button cancelButton = (Button) findViewById(R.id.eb_back);
        cancelButton.setOnClickListener(cancelButtonListener);

        ImageView flipButton = (ImageView) findViewById(R.id.image_solve_button1);
        flipButton.setOnTouchListener(imageTouchListener);

        ImageView hintButton = (ImageView) findViewById(R.id.image_solve_button2);
        hintButton.setOnTouchListener(imageTouchListener);

        startButton = (Button) findViewById(R.id.eb_ok2);
        startButton.setOnClickListener(startButtonListener);

        gameText = (TextView) findViewById(R.id.chess_notation_solve_text);
        gameAnnotation = new Annotations(gameText);

        questionText = (TextView) findViewById(R.id.solve_title_text);

        promoDialog = new Dialog(this);
        promoDialog.setCancelable(false);
        promoDialog.setContentView(R.layout.promotion_dialog);

        if(getIntent().getStringExtra("pgnFile")!=null)
        {
            pgn = getIntent().getStringExtra("pgnFile");
        }
        if(getIntent().getStringExtra("filename")!=null)
        {
            filename = getIntent().getStringExtra("filename");
        }

        gameText.setText("Press 'Start' to begin solving the exercise ");
        gameText.setGravity(View.TEXT_ALIGNMENT_CENTER);
    }

    private View.OnClickListener cancelButtonListener = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            finish();
        }
    };

    private View.OnClickListener startButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            if(exerciseDone)
            {
                finish();
            }
            else
            {
                setSavedPosition();
                setAnnotation();
                startButton.setClickable(false);
            }
        }
    };

    private View.OnTouchListener imageTouchListener = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View v, MotionEvent event)
        {
            switch(event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                {
                    ImageView view = (ImageView) v;
                    view.getDrawable().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    view.invalidate();
                    break;
                }

                case MotionEvent.ACTION_UP:
                    int id = v.getId();
                    switch(id)
                    {
                        case R.id.image_solve_button1:
                        {
                            if(cbview.getFlipped())
                            {
                                cbview.setFlipped(false);
                            }
                            else
                            {
                                cbview.setFlipped(true);
                            }
                            break;
                        }
                        case R.id.image_solve_button2:
                        {
                            String hint = PgnHelper.getHintField(pgn);
                            Toast t;
                            if(hint == null || hint.isEmpty())
                            {
                                t = Toast.makeText(mContext,"No hint...",Toast.LENGTH_SHORT);
                            }
                            else
                            {
                                t = Toast.makeText(mContext,hint,Toast.LENGTH_SHORT);
                            }
                            t.show();
                            break;
                        }
                    }

                case MotionEvent.ACTION_CANCEL:
                {
                    ImageView view = (ImageView) v;
                    view.getDrawable().clearColorFilter();
                    view.invalidate();
                    break;
                }
            }
            return true;
        }
    };

    private ChessboardGame.DidDrawListener drawEndedListener = new ChessboardGame.DidDrawListener() {
        @Override
        public boolean onDrawEnded() {
            return false;
        }
    };

    private ChessboardGame.PieceToMoveEventListener moveEventListener = new ChessboardGame.PieceToMoveEventListener() {
        @Override
        public void onEventOccured(ChessPiece cp, String xfrom, String yfrom, String xto, String yto)
        {
            if(playerCanPlay)
            {
                if(chessboardController.makeMove(cp, Integer.parseInt(xfrom), ViewToBoardTools.stringToCoord(yfrom), Integer.parseInt(xto), ViewToBoardTools.stringToCoord(yto)))
                {
                    // Check if pawn is promoting
                    if(chessboardController.isPromoting())
                    {
                        promoDialog.show();
                        ImageView hetman = (ImageView) promoDialog.findViewById(R.id.promocja_hetman);
                        ImageView wieza = (ImageView) promoDialog.findViewById(R.id.promocja_wieza);
                        ImageView goniec = (ImageView) promoDialog.findViewById(R.id.promocja_goniec);
                        ImageView skoczek = (ImageView) promoDialog.findViewById(R.id.promocja_kon);
                        if(!chessboardController.getPromoColour())
                        {
                            hetman.setImageResource(R.drawable.czarny_hetman);
                            wieza.setImageResource(R.drawable.czarna_wieza);
                            goniec.setImageResource(R.drawable.czarny_goniec);
                            skoczek.setImageResource(R.drawable.czarny_kon);
                        }
                        hetman.setOnClickListener(new PromoteListener(ChessPieces.QUEEN));
                        wieza.setOnClickListener(new PromoteListener(ChessPieces.ROOK));
                        goniec.setOnClickListener(new PromoteListener(ChessPieces.BISHOP));
                        skoczek.setOnClickListener(new PromoteListener(ChessPieces.KNIGHT));

                        promotionfromx = xfrom;
                        promotionfromy = yfrom;
                        promotiontox = xto;
                        promotiontoy = yto;

                    }
                    else
                    {
                        checkIfAnswerCorrect();
                    }
                }
                else
                {
                    // Invalid move!
                }
            }
        }
    };

    private class PromoteListener implements View.OnClickListener
    {
        ChessPieces mypiece;
        public PromoteListener(ChessPieces piece)
        {
            mypiece = piece;
        }

        @Override
        public void onClick(View v)
        {
            chessboardController.promote(mypiece);
            checkIfAnswerCorrect();
            promoDialog.dismiss();
        }
    }

    private void setSavedPosition()
    {
        String fen = PgnHelper.getFenField(pgn);
        chessboardController = new ChessboardController(PgnHelper.fenToPosition(fen));
        for(int i=0; i<8; i++)
        {
            for(int k=0; k<8; k++)
            {
                ChessPiece piece = chessboardController.getSquare(i,k);
                if(piece!=null)
                {
                    cbview.setPiece(piece.getType(), piece.getColor(), String.valueOf(i), ViewToBoardTools.coordToString(k));
                }
            }
        }
        gameText.setText("");
        questionText.setText(PgnHelper.getQuestionField(pgn));

        cbview.redraw();
        gameAnnotation.addInitialPosition(chessboardController);
    }

    // Set a new position on Chessboard
    private void setNewPosition()
    {
        ChessPiece [][] position = new ChessPiece[8][8];
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                position[i][j] = chessboardController.getSquare(i,j);
            }
        }
        cbview.setPieces(position);
    }

    private void setAnnotation()
    {
        String currentGameText = PgnHelper.getAnnotationsField(pgn);
        int moveCounter = 1;
        boolean nowMoving = true;
        boolean nowFirst = true;
        boolean moveChanger = true;
        int totalMoves = 0;
        int phaseCounter = 0;
        int start = 0;
        String [] movePhase = {"moveNr", "moveW", "moveB"};
        boolean moveValid = false;

        ArrayList<Integer> moves = new ArrayList<>();
        Map<Integer, String> moveTexts = new HashMap<>();
        Map<Integer, ChessboardController> movePositions = new HashMap<>();

        ChessboardController mycc = null;

        String themove = "";

        try
        {
            mycc = (ChessboardController) chessboardController.clone();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String [] movesSplit = currentGameText.split(" ");
        if(movesSplit[0].equals("1."))
        {
            nowMoving = true;
            nowFirst = true;
            moveCounter = 1;
            chessboardController.setOnMove(true);
            mycc.setOnMove(true);
        }
        else
        {
            nowMoving = false;
            nowFirst = false;
            moveCounter = -1;
            chessboardController.setOnMove(false);
            mycc.setOnMove(false);
            moveChanger = false;
        }
        for(int i=0; i<movesSplit.length; i++)
        {
            if(!movesSplit[i].equals("1-0") && !movesSplit[i].equals("0-1") && !movesSplit[i].equals("\n"))
            {
                themove = themove + movesSplit[i];
                if(movePhase[phaseCounter].equals("moveNr"))
                {
                    themove = themove + " ";
                    phaseCounter++;
                    if(!moveChanger)
                    {
                        phaseCounter++;
                        moveChanger = true;
                    }
                }
                else if(movePhase[phaseCounter].equals("moveW"))
                {
                    themove = themove + " ";

                    // Moves
                    moves.add(moveCounter);

                    // Move texts
                    moveTexts.put(moveCounter,themove);

                    // Move positions
                    ChessPiece chessPiece = PgnHelper.getPieceFromNotation(movesSplit[i],nowMoving);
                    String shortmove = PgnHelper.getMoveFromNotation(movesSplit[i]);
                    if(shortmove.equals("0-0"))
                    {
                        moveValid = mycc.makeMove(chessPiece, 0, 6, '-', nowMoving);
                    }
                    else if(shortmove.equals("0-0-0"))
                    {
                        moveValid = mycc.makeMove(chessPiece, 0, 2, '-', nowMoving);
                    }
                    else
                    {
                        if(shortmove.length()>2)
                        {
                            char cextra = shortmove.charAt(0);
                            char c1 = shortmove.charAt(1);
                            char c2 = shortmove.charAt(2);
                            moveValid = mycc.makeMove(chessPiece, Integer.parseInt(String.valueOf(c2))-1, ViewToBoardTools.stringToCoord(String.valueOf(c1)), cextra, nowMoving);

                        }
                        else
                        {
                            char c1 = shortmove.charAt(0);
                            char c2 = shortmove.charAt(1);
                            moveValid = mycc.makeMove(chessPiece,Integer.parseInt(String.valueOf(c2))-1,ViewToBoardTools.stringToCoord(String.valueOf(c1)),'-', nowMoving);
                        }
                    }

                    try
                    {
                        ChessboardController cc = (ChessboardController) mycc.clone();
                        movePositions.put(moveCounter,cc);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    moveCounter = moveCounter * (-1);
                    phaseCounter++;
                    themove = "";
                    nowMoving = false;
                    totalMoves++;
                }
                else
                {
                    themove = themove + " ";

                    // Moves
                    moves.add(moveCounter);

                    // Move texts
                    moveTexts.put(moveCounter,themove);

                    // Move positions
                    ChessPiece chessPiece = PgnHelper.getPieceFromNotation(movesSplit[i],nowMoving);
                    String shortmove = PgnHelper.getMoveFromNotation(movesSplit[i]);
                    if(shortmove.equals("0-0"))
                    {
                        moveValid = mycc.makeMove(chessPiece, 7, 6, 'g', nowMoving);
                    }
                    else if(shortmove.equals("0-0-0"))
                    {
                        moveValid = mycc.makeMove(chessPiece, 7, 2, 'c', nowMoving);
                    }
                    else
                    {
                        if(shortmove.length()>2)
                        {
                            char cextra = shortmove.charAt(0);
                            char c1 = shortmove.charAt(1);
                            char c2 = shortmove.charAt(2);
                            moveValid = mycc.makeMove(chessPiece,Integer.parseInt(String.valueOf(c2))-1,ViewToBoardTools.stringToCoord(String.valueOf(c1)),cextra, nowMoving);
                        }
                        else
                        {
                            char c1 = shortmove.charAt(0);
                            char c2 = shortmove.charAt(1);
                            moveValid = mycc.makeMove(chessPiece,Integer.parseInt(String.valueOf(c2))-1,ViewToBoardTools.stringToCoord(String.valueOf(c1)),'-', nowMoving);
                        }
                    }
                    try
                    {
                        ChessboardController cc = (ChessboardController) mycc.clone();
                        movePositions.put(moveCounter,cc);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    moveCounter = Math.abs(moveCounter) + 1;
                    phaseCounter = 0;
                    themove = "";
                    nowMoving = true;
                }
            }
            else
            {
                break;
            }
        }
        // Single move or variation
        String var = PgnHelper.getVariationField(pgn);
        if(var.equals("single"))
        {
            isVariation = false;
        }
        else
        {
            isVariation = true;
        }

        // Who goes first
        if(nowFirst)
        {
            gameAnnotation.setInitialAnnotation(currentGameText, moves, moveTexts, movePositions, 1, totalMoves);
        }
        else
        {
            gameAnnotation.setInitialAnnotation(currentGameText, moves, moveTexts, movePositions, -1, totalMoves);
        }
        gameText.setText("Setting up position");
        String combinationOnMove = PgnHelper.getMoveNrField(pgn);
        String colorOnMove = PgnHelper.getOnMoveField(pgn);
        String timer = PgnHelper.getTimeField(pgn);
        int m = Integer.valueOf(timer.substring(0,timer.lastIndexOf(':')));
        int s = Integer.valueOf(timer.substring(timer.lastIndexOf(':')+1,timer.length()));
        minutes = Long.valueOf(m);
        seconds = Long.valueOf(s);
        moveSlowlyTo(combinationOnMove, colorOnMove);
    }

    private void moveSlowlyTo(String moveNr, String onMove)
    {
        if(onMove.equals("white"))
        {
            untilmove = Integer.parseInt(moveNr);
        }
        else
        {
            untilmove = (-1) * Integer.parseInt(moveNr);
        }
        currentlyOnMove = chessboardController.getOnMove();
        stopLoop = false;
        moveshandler = new Handler();
        moveshandler.postDelayed(gotomove, 500);
    }

    private Runnable gotomove = new Runnable()
    {
        @Override
        public void run()
        {
            ChessboardController cc = null;
            int key = gameAnnotation.getMovesKey(currentlyOnMove);
            if(key==untilmove || stopLoop)
            {
                // STOP!
                playerCanPlay = true;
                gameText.setText("Insert correct move!");
                if(minutes==0L && seconds==0L)
                {
                    startButton.setText(DecimalFormatSymbols.getInstance().getInfinity());
                }
                else
                {
                    solveTimer = new CountDownTimer((minutes*60000)+(seconds*1000), 1000) {
                        @Override
                        public void onTick(long l) {
                            if((l/1000)%60 < 10)
                            {
                                startButton.setText((l/60000)+":0"+((l/1000%60)));
                            }
                            else
                            {
                                startButton.setText((l/60000)+":"+((l/1000)%60));
                            }
                        }

                        @Override
                        public void onFinish() {
                            playerCanPlay = false;
                            Toast toast = Toast.makeText(mContext, "Time's up!", Toast.LENGTH_SHORT);
                            toast.show();
                            exerciseDone = true;
                            startButton.setText("OK");
                            startButton.setClickable(true);
                        }
                    };
                    solveTimer.start();
                }

            }
            else
            {
                gameText.setVisibility(View.INVISIBLE);
                cc = gameAnnotation.moveForward(currentlyOnMove);

                currentlyOnMove = !currentlyOnMove;

                if(gameAnnotation.getMovesKey(currentlyOnMove)==untilmove)
                {
                    cc = gameAnnotation.moveBack(currentlyOnMove);
                    currentlyOnMove = !currentlyOnMove;
                    stopLoop = true;
                }

                try
                {
                    chessboardController = (ChessboardController) cc.clone();
                }
                catch (CloneNotSupportedException e)
                {
                    e.printStackTrace();
                }
                setNewPosition();
                cbview.redraw();
                gameText.setText(settingUp[settingUpCounter]);
                if(settingUpCounter<3)
                {
                    settingUpCounter++;
                }
                else
                {
                    settingUpCounter = 0;
                }
                gameText.setVisibility(View.VISIBLE);
                moveshandler.postDelayed(this,1000);
            }
        }
    };

    private Runnable finishMoves = new Runnable()
    {
        @Override
        public void run()
        {
            ChessboardController cc = gameAnnotation.moveForward(chessboardController.getOnMove());
            if(cc!=null)
            {
                if(comparePositions(cc,chessboardController))
                {
                    try
                    {
                        chessboardController = (ChessboardController) cc.clone();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    setNewPosition();
                    cbview.redraw();
                    moveshandler.postDelayed(this, 1000);
                }
            }
        }
    };

    private Runnable playMove = new Runnable() {
        @Override
        public void run()
        {
            try
            {
                chessboardController = (ChessboardController) moveup.clone();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            setNewPosition();
            cbview.redraw();
            gameText.setText("Insert correct move");
        }
    };

    private void checkIfAnswerCorrect()
    {
        boolean different = false;
        ChessboardController cc = gameAnnotation.ccOnCurrentMove(!chessboardController.getOnMove());
       different = comparePositions(cc,chessboardController);

        if(different)
        {
            Toast toast = Toast.makeText(mContext, "Wrong answer!", Toast.LENGTH_SHORT);
            toast.show();
            cc = gameAnnotation.moveBack(chessboardController.getOnMove());
            try
            {
                chessboardController = (ChessboardController) cc.clone();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            gameText.setText("Insert correct move");
        }
        else
        {
            if(isVariation)
            {
                ChessboardController ccn = gameAnnotation.moveForward(!chessboardController.getOnMove());
                try
                {
                    chessboardController = (ChessboardController) ccn.clone();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                setNewPosition();
                ccn = gameAnnotation.moveForward(chessboardController.getOnMove());

                if(ccn!=null)
                {
                    moveup = ccn;
                    gameText.setText("Opponent's move, wait");
                    moveshandler.postDelayed(playMove,1000);
                }
                else
                {
                    setNewPosition();
                    Toast toast = Toast.makeText(mContext, "Your variation is correct!", Toast.LENGTH_SHORT);
                    toast.show();
                    exerciseDone = true;
                    solveTimer.cancel();

                    cbview.redraw();
                    startButton.setClickable(true);
                    startButton.setText("OK!");
                    String solved = sharedPreferences.getString("solved_exercises","");
                    String addition = filename.substring(0,filename.lastIndexOf("."));
                    solved = solved + addition +"#";
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("solved_exercises",solved);
                    editor.commit();
                }

            }
            else
            {
                setNewPosition();
                Toast toast = Toast.makeText(mContext, "Your answer is correct!", Toast.LENGTH_SHORT);
                toast.show();
                exerciseDone = true;
                solveTimer.cancel();

                cbview.redraw();
                startButton.setClickable(true);
                startButton.setText("OK!");

                gameAnnotation.moveForward(!chessboardController.getOnMove());
                moveshandler.postDelayed(finishMoves, 500);

                String solved = sharedPreferences.getString("solved_exercises","");
                String addition = filename.substring(0,filename.lastIndexOf("."));
                solved = solved + addition +"#";
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("solved_exercises",solved);
                editor.commit();
            }
        }
    }

    private boolean comparePositions(ChessboardController cc1, ChessboardController cc2)
    {
        boolean different = false;
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(cc2.getSquare(i,j)==null)
                {
                    if(cc1.getSquare(i,j)!=null)
                    {
                        different = true;
                        break;
                    }
                }
                else
                {
                    if(cc1.getSquare(i,j)==null)
                    {
                        different = true;
                        break;
                    }
                    else
                    {
                        if(cc2.getSquare(i,j).getType() == cc1.getSquare(i,j).getType() && cc2.getSquare(i,j).getColor() == cc1.getSquare(i,j).getColor())
                        {
                            // It's ok
                        }
                        else
                        {
                            different = true;
                            break;
                        }
                    }
                }
            }
        }
        return different;
    }
}
