package com.hetman.chesscoach.services;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by shevson on 6/3/17.
 */

public class DatabaseConnector
{
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "chess_exercises.db";
    private static final String DB_EXERCISES_TABLE = "exercises";

    private SQLiteDatabase db;
    private Context mContext;
    private DatabaseHelper helper;

    public DatabaseConnector(Context mContext)
    {
        this.mContext = mContext;
    }

    // Open the database
    public DatabaseConnector openDB()
    {
        helper = new DatabaseHelper(mContext,DB_NAME,null,DB_VERSION);
        try
        {
            db = helper.getWritableDatabase();
        }
        catch (SQLException e)
        {
            db = helper.getReadableDatabase();
        }

        return this;
    }

    public void closeDB()
    {
        helper.close();
    }

    // Return all exercises
    public Cursor getAll()
    {
        return db.rawQuery("select * from "+DB_EXERCISES_TABLE, null);
    }

    // Return filtered exercises
    public Cursor getAllFiltered(ArrayList<String> filters)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from "+DB_EXERCISES_TABLE+" where level=");
        for(int k=0; k<filters.size(); k++)
        {
            if(k+1==filters.size())
            {
                sb.append("'"+filters.get(k)+"'");
            }
            else
            {
                sb.append("'"+filters.get(k)+"' or level=");
            }
        }
        String query = sb.toString();
        return db.rawQuery(query, null);
    }

    // DatabaseHelper - from library SQLiteAssetHelper
    private static class DatabaseHelper extends SQLiteAssetHelper
    {
        public DatabaseHelper (Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
        {
            super(context, name, factory, version);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            // Not needed
        }
    }
}
