package com.hetman.chesscoach.services;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.icu.util.Output;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;

/**
 * Created by shevson on 5/24/17.
 */

public class BluetoothService
{
    private static final String TAG = "BluetoothConnectionService";
    private static final String APPNAME = "ChessCoach";
    private static final UUID MY_UUID = UUID.fromString("26c39f0b-5ad2-48f1-9745-45c7589c12f8");

    private final BluetoothAdapter mBluetoothAdapter;
    Context mContext;

    private AcceptBluetoothThread mInsecureAcceptBluetoothThread;
    private ConnectBluetoothThread mConnectBluetoothThread;
    private ConnectedBluetoothThread mConnectedBluetoothThread;
    private BluetoothDevice mDevice;

    private UUID deviceUUID;

    public Boolean isConnected = false;

    private SaveFileListener sfl;

    public interface SaveFileListener {
        void onSaveFile(String filename, String pgn);
    }

    public void setSaveFileListener (SaveFileListener sfl)
    {
        this.sfl = sfl;
    }

    public BluetoothService(Context context, boolean isServer)
    {
        this.mContext = context;
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(isServer)
        {
            startService();
            Log.d("BS","Service started");
        }
    }

    // Thread for listening for incoming connections (like a server)
    private class AcceptBluetoothThread extends Thread
    {
        private BluetoothServerSocket mBluetoothServerSocket;

        public AcceptBluetoothThread()
        {
            BluetoothServerSocket bss = null;
            try
            {
                bss = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(APPNAME, MY_UUID);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            mBluetoothServerSocket = bss;
        }

        // Run method for Thread
        public void run()
        {
            BluetoothSocket socket = null;
            try
            {
                Log.d("BS","Socket waiting for connection");
                socket = mBluetoothServerSocket.accept();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            if(socket!=null)
            {
                bluetoothConnected(socket, mDevice);
            }
        }

        // Cancel method for Thread
        public void cancel()
        {
            try
            {
                mBluetoothServerSocket.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    // Thread for attempting to connect with a device
    private class ConnectBluetoothThread extends Thread
    {
        private BluetoothSocket socket;

        public ConnectBluetoothThread(BluetoothDevice device, UUID uuid)
        {
            mDevice = device;
            deviceUUID = uuid;
        }

        // Run method for Thread
        public void run()
        {
            BluetoothSocket tmp_socket = null;

            try
            {
                tmp_socket = mDevice.createRfcommSocketToServiceRecord(deviceUUID);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            socket = tmp_socket;

            mBluetoothAdapter.cancelDiscovery();
            Log.d("BS","Trying to connect to a socket");
            try
            {
                socket.connect();
                bluetoothConnected(socket, mDevice);
            }
            catch (IOException e)
            {
                e.printStackTrace();
                try
                {
                    socket.close();
                }
                catch (IOException f)
                {
                    f.printStackTrace();
                }
            }
        }

        // Cancel method for Thread
        public void cancel()
        {
            try
            {
                socket.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    // Thread for connected bluetooth service
    private class ConnectedBluetoothThread extends Thread
    {
        private final BluetoothSocket socket;
        private final InputStream inputStream;
        private final OutputStream outputStream;

        public ConnectedBluetoothThread(BluetoothSocket socket)
        {
            isConnected = true;
            this.socket = socket;
            InputStream is = null;
            OutputStream os = null;

            try
            {
                is = socket.getInputStream();
                os = socket.getOutputStream();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            inputStream = is;
            outputStream = os;
        }

        // Run method for Thread
        public void run()
        {
            byte[] buffer = new byte[1024];
            int bytes;

            String pgn = "";
            String filename = "";
            int k = 0;

            while(true)
            {
                try
                {
                    bytes = inputStream.read(buffer);
                    String incomingMessage = new String(buffer, 0, bytes);
                    if(incomingMessage.contains(".pgn"))
                    {
                        k = incomingMessage.lastIndexOf(".pgn");
                        filename = incomingMessage.substring(0,k+4);
                        pgn = incomingMessage.substring(k+5);
                        if(sfl!=null)
                        {
                            sfl.onSaveFile(filename, pgn);
                        }
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    break;
                }

            }
        }

        // Cancel method for Thread
        public void cancel()
        {
            try
            {
                socket.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }

        public void write(byte[] bytes)
        {
            String text = new String(bytes, Charset.defaultCharset());
            try
            {
                outputStream.write(bytes);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    // When connection is established
    private void bluetoothConnected(BluetoothSocket socket, BluetoothDevice device)
    {
        mConnectedBluetoothThread = new ConnectedBluetoothThread(socket);
        mConnectedBluetoothThread.start();
    }

    // Start the service
    public synchronized void startService()
    {
        if(mConnectBluetoothThread != null)
        {
            mConnectBluetoothThread.cancel();
            mConnectBluetoothThread = null;
        }

        if(mInsecureAcceptBluetoothThread == null)
        {
            mInsecureAcceptBluetoothThread = new AcceptBluetoothThread();
            mInsecureAcceptBluetoothThread.start();
        }
    }

    // Start AcceptThread and wait for connection
    public void startClient(BluetoothDevice device, UUID uuid)
    {
        mConnectBluetoothThread = new ConnectBluetoothThread(device, uuid);
        mConnectBluetoothThread.start();
    }

    public void stopClient()
    {
        if(mConnectBluetoothThread != null)
        {
            mConnectBluetoothThread.cancel();
            mConnectBluetoothThread = null;
        }

        if(mInsecureAcceptBluetoothThread != null)
        {
            mInsecureAcceptBluetoothThread.cancel();
            mInsecureAcceptBluetoothThread = null;
        }
    }

    // For access from another class
    public void write(byte[] out)
    {
        mConnectedBluetoothThread.write(out);
    }
}
