package com.hetman.chesscoach.annotations;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.widget.TextView;

import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.tools.PgnHelper;
import com.hetman.chesscoach.values.ChessPieces;
import com.hetman.chesscoach.values.pieces.ChessPiece;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shevson on 5/2/17.
 */

public class Annotations
{
    TextView gameText;
    int movesCounter;
    int maxMoves;
    String currentGameText;
    Spanned currentGameSpanned;
    boolean firstMove = true;
    ChessPiece [][] initialPosition = new ChessPiece[8][8];

    ChessboardController initialController;
    ArrayList<Integer> moves;
    Map<Integer, ChessboardController> movePositions;
    Map<Integer, String> moveTexts;

    String pgn = "";
    String finalpgn = "";

    // Constructor
    public Annotations(TextView gameText)
    {
        this.gameText = gameText;
        movesCounter = 1;
        maxMoves = movesCounter;
        currentGameText = "";
        currentGameSpanned = null;
        moves = new ArrayList<>();
        movePositions = new HashMap<>();
        moveTexts = new HashMap<>();
    }

    // Adding initial position
    public void addInitialPosition(ChessboardController cc)
    {
        try
        {
            initialController = (ChessboardController) cc.clone();
        }
        catch(CloneNotSupportedException e)
        {
            e.printStackTrace();
        }

        for(int x=0; x<8; x++)
        {
            for(int y=0; y<8; y++)
            {
                initialPosition[x][y] = cc.getSquare(x,y);
            }
        }
    }

    // Adding annotationg from PGN file
    public void setInitialAnnotation(String currentGameText, ArrayList<Integer> moves, Map<Integer,String> moveTexts, Map<Integer,ChessboardController> movePositions, int nowOnMove, int maxMoves)
    {
        this.currentGameText = currentGameText;
        this.moves = moves;
        this.moveTexts = moveTexts;
        this.movePositions = movePositions;
        this.maxMoves = maxMoves;
        if(nowOnMove==1)
        {
            initialController.setOnMove(true);
        }
        else
        {
            initialController.setOnMove(false);
        }
        Spanned finalSpanned = Html.fromHtml(getCurrentAnnotationWihtKey(0));
        gameText.setText(finalSpanned);
    }

    // Add new move to PGN notation
    public void addMove(ChessboardController cc, ChessPiece cp, String xfrom, String yfrom, String xto, String yto, boolean isTaking, boolean isChecking, boolean wasCastled, boolean onMove, boolean wasMated, ChessPieces promoPiece, String coordinate)
    {
        String moveNumber = "";
        String piece = "";
        String squareTo = "";
        String check = "";
        String taking = "";
        String castle = "";
        String mated = "";
        String winner = "";
        String promoted = "";

        String finalText = "";
        Spanned finalSpanned = null;
        int movePos = 0;

        // Set move pos
        if(!onMove)
        {
            movePos = movesCounter;
        }
        else
        {
            movePos = -(movesCounter);
        }

        // Check if move exists - remove if user wishes so
        if(checkIfMoveExists(onMove))
        {
            moves.indexOf(movePos);
            int size = moves.size()-1;
            int index = moves.indexOf(movePos);
            ArrayList<Integer> keysToRemove = new ArrayList<>();
            for(int i=size; i>=index; i--)
            {
                keysToRemove.add(moves.get(i));
                moves.remove(i);
            }
            for(int j=0; j<keysToRemove.size(); j++)
            {
                if(movePositions.containsKey(keysToRemove.get(j)))
                {
                    movePositions.remove(keysToRemove.get(j));
                }
                if(moveTexts.containsKey(keysToRemove.get(j)))
                {
                    moveTexts.remove(keysToRemove.get(j));
                }
            }
        }

        // Increase x coordinate
        int x = Integer.valueOf(xto);
        x += 1;
        String newxto = String.valueOf(x);

        // Set piece
        switch(cp.getType())
        {
            case PAWN:
                if(isTaking)
                {
                    piece = yfrom;
                }
                else
                {
                    piece = "";
                }
                break;
            case KNIGHT: piece = "S"; break;
            case BISHOP: piece = "G"; break;
            case ROOK: piece = "W"; break;
            case QUEEN: piece = "H"; break;
            case KING: piece = "K"; break;
            default: piece = ""; break;
        }

        if(promoPiece!=null)
        {
            switch(promoPiece)
            {
                case KNIGHT: promoted = "=S"; break;
                case BISHOP: promoted = "=G"; break;
                case ROOK: promoted = "=W"; break;
                case QUEEN: promoted = "=H"; break;
                default: promoted = ""; break;
            }
        }

        // Set where to
        squareTo = yto+(newxto);

        // Set move number
        if(onMove == true)
        {
            if(firstMove)
            {
                moveNumber = String.valueOf(movesCounter)+"... ";
                firstMove = false;
                initialController.setOnMove(false);
                movesCounter++;
            }
            else
            {
                movesCounter++;
            }
        }
        else
        {
            if(firstMove)
            {
                firstMove = false;
                initialController.setOnMove(true);
            }
            moveNumber = String.valueOf(movesCounter)+". ";
        }

        // Set check
        if(isChecking)
        {
            if(wasMated)
            {
                mated = "#";
                if(onMove)
                {
                    winner = "\n 0-1";
                }
                else
                {
                    winner = "\n 1-0";
                }
            }
            else
            {
                check = "+";
            }
        }

        if(isTaking)
        {
            taking = "x";
        }

        // Set castled
        if(wasCastled)
        {
            if(yto.equals("g"))
            {
                castle = "0-0";
            }
            else
            {
                castle = "0-0-0";
            }

            finalText = (moveNumber+castle+check+mated+winner+" ");
            moves.add(movePos);
            moveTexts.put(movePos, finalText);
            currentGameText = getCurrentAnnotation();
        }
        else
        {
            finalText = (moveNumber+piece+coordinate+taking+squareTo+promoted+check+mated+winner+" ");
            moves.add(movePos);
            moveTexts.put(movePos, finalText);
            currentGameText = getCurrentAnnotation();
        }

        finalSpanned = Html.fromHtml(getCurrentAnnotationWihtKey(movePos));

        currentGameSpanned = finalSpanned;
        gameText.setText(currentGameSpanned);

        // Adding move positions
        ChessboardController mycc = null;
        try
        {
            mycc = (ChessboardController) cc.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        movePositions.put(movePos,mycc);

        // Max moves
        maxMoves = movesCounter;
    }

    // Check if move exists in PGN notation
    public boolean checkIfMoveExists(boolean onMove)
    {
        int movePos;
        if(!onMove)
        {
            movePos = movesCounter;
        }
        else
        {
            movePos = -(movesCounter);
        }

        if(moves.contains(movePos))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Save current annotations to PGN file
    public boolean saveAnnotationsToPGN(String name, String onMove, int moveNr, String level, boolean singleMove, boolean wholeVar, String question, String hint, int minutes, int seconds, Context ctx)
    {
        boolean isValid = false;
        String startBracket = "[";
        String endBracket = "]\n";
        StringBuilder stringBuilder = new StringBuilder();

        if(moveNr!=0 && !question.isEmpty() && !name.isEmpty())
        {
            if(!singleMove && !wholeVar)
            {
               isValid = false;
            }
            else
            {
                isValid = true;
                // Add OnMove
                stringBuilder.append(startBracket);
                stringBuilder.append("OnMove ");
                stringBuilder.append("\""+onMove+"\"");
                stringBuilder.append(endBracket);

                // Add MoveNr
                stringBuilder.append(startBracket);
                stringBuilder.append("MoveNr ");
                stringBuilder.append("\""+moveNr+"\"");
                stringBuilder.append(endBracket);

                // Add Question
                stringBuilder.append(startBracket);
                stringBuilder.append("Question ");
                stringBuilder.append("\""+question+"\"");
                stringBuilder.append(endBracket);

                // Add Hint
                stringBuilder.append(startBracket);
                stringBuilder.append("Hint ");
                stringBuilder.append("\""+hint+"\"");
                stringBuilder.append(endBracket);

                // Add level
                stringBuilder.append(startBracket);
                stringBuilder.append("Level ");
                stringBuilder.append("\""+level+"\"");
                stringBuilder.append(endBracket);

                // Single Move or Variation
                if(singleMove)
                {
                    stringBuilder.append(startBracket);
                    stringBuilder.append("Answer ");
                    stringBuilder.append("\"single\"");
                    stringBuilder.append(endBracket);
                }
                else if(wholeVar)
                {
                    stringBuilder.append(startBracket);
                    stringBuilder.append("Answer ");
                    stringBuilder.append("\"variation\"");
                    stringBuilder.append(endBracket);
                }

                // Minutes and seconds
                stringBuilder.append(startBracket);
                stringBuilder.append("Timer ");
                stringBuilder.append("\""+minutes+":"+seconds+"\"");
                stringBuilder.append(endBracket);

                // FEN position
                stringBuilder.append(startBracket);
                stringBuilder.append("FEN ");
                stringBuilder.append("\""+PgnHelper.positionToFEN(initialPosition)+"\"");
                stringBuilder.append(endBracket);
            }
        }

        pgn = stringBuilder.toString();

        if(!pgn.isEmpty())
        {
            saveToFile(ctx, name);
        }

        return isValid;
    }

    // Move back accordingly to notation
    public ChessboardController moveBack(boolean onMove)
    {
        ChessboardController cc = null;
        int mykey = 0;
        if(onMove)
        {
           mykey = movesCounter-1;
        }
        else
        {
            mykey = -(movesCounter-1);
        }

        if(movesCounter==1 && !onMove)
        {
            cc = initialController;
        }
        else
        {
            for(Integer key:movePositions.keySet())
            {
                if(key == mykey)
                {
                    cc = movePositions.get(key);
                    break;
                }
            }
        }

        if(movesCounter==2 && onMove && cc==null)
        {
            cc = initialController;
        }


        if(cc!=null)
        {
            if(onMove && movesCounter>1)
            {
                movesCounter--;
            }
            currentGameSpanned = Html.fromHtml(getCurrentAnnotationWihtKey(mykey));
            gameText.setText(currentGameSpanned);
        }
        return cc;
    }

    // Move forward accordingly to PGN notation
    public ChessboardController moveForward(boolean onMove)
    {
        ChessboardController cc = null;
        int mykey = 0;
        if(onMove)
        {
            mykey = movesCounter;
        }
        else
        {
            mykey = -(movesCounter);
        }

        for(int key: movePositions.keySet())
        {
            if(key == mykey)
            {
                cc = movePositions.get(key);
                break;
            }
        }

        if(cc!=null)
        {
            if(!onMove && movesCounter<=maxMoves+1)
            {
                movesCounter++;
            }
            currentGameSpanned = Html.fromHtml(getCurrentAnnotationWihtKey(mykey));
            gameText.setText(currentGameSpanned);
        }
        return cc;
    }

    // Get position on current move accordingly to PGN notation
    public ChessboardController ccOnCurrentMove(boolean onMove)
    {
        ChessboardController cc = null;
        int mykey = 0;
        if(onMove)
        {
            mykey = movesCounter;
        }
        else
        {
            mykey = -(movesCounter);
        }
        for(int key: movePositions.keySet())
        {
            if(key == mykey)
            {
                cc = movePositions.get(key);
                break;
            }
        }

        if(cc == null && Math.abs(movesCounter)==1)
        {
            cc = initialController;
        }
        return cc;
    }

    // Return current annotation (String) on specific move
    private String getCurrentAnnotationWihtKey(int key)
    {
        String ann = "";
        StringBuilder sb = new StringBuilder();
        if(!moveTexts.isEmpty() && !moves.isEmpty())
        {
            for(int i=0; i<moves.size(); i++)
            {
                for(int mykey: moveTexts.keySet())
                {
                    if(moves.get(i)==mykey)
                    {
                        if(mykey==key)
                        {
                            sb.append("<b>"+moveTexts.get(mykey)+"</b>");
                        }
                        else
                        {
                            sb.append(moveTexts.get(mykey));
                        }
                    }
                }
            }
        }
        ann = sb.toString();
        return ann;
    }

    // Get current annotation (String)
    private String getCurrentAnnotation()
    {
        String ann = "";
        StringBuilder sb = new StringBuilder();
        if(!moveTexts.isEmpty() && !moves.isEmpty())
        {
            for(int i=0; i<moves.size(); i++)
            {
                for(int thekey: moveTexts.keySet())
                {
                    if(moves.get(i)==thekey)
                    {
                        sb.append(moveTexts.get(thekey));
                    }
                }
            }
        }
        ann = sb.toString();
        return ann;
    }

    // Return how many moves were made
    public int getMovesCounter()
    {
        return movesCounter;
    }

    // Get move accordingly to current player (white/black)
    public int getMovesKey(boolean onMove)
    {
        if(onMove)
        {
            return movesCounter;
        }
        else
        {
            return -(movesCounter);
        }
    }

    // Save generated annotations to file
    private void saveToFile(Context ctx, String filename)
    {
        String mydir = ctx.getDir("exercises", Context.MODE_PRIVATE).getPath();
        File file = new File(mydir, filename+".pgn");
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(file);
            fos.write((pgn+"\n"+currentGameText).getBytes());
            fos.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}