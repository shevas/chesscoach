package com.hetman.chesscoach;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.hetman.chesscoach.services.BluetoothService;

import java.io.File;
import java.io.FileOutputStream;


public class MainScreen extends AppCompatActivity
{

    AlertDialog exitDialog;
    BluetoothAdapter bluetoothAdapter;
    ImageView bluetoothButton;
    BluetoothService btService;

    BluetoothDevice deviceToConnect;

    boolean isReceiver1;
    boolean isReceiver2;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState); 

        setContentView(R.layout.activity_main_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_screen_toolbar);
        setSupportActionBar(toolbar);

        Button editChessboard = (Button) findViewById(R.id.edit_board_button);
        editChessboard.setOnClickListener(toChessEditor);

        Button newChessboard = (Button) findViewById(R.id.new_game_button);
        newChessboard.setOnClickListener(toChessEditor);

        Button myExercises = (Button) findViewById(R.id.my_games_button);
        myExercises.setOnClickListener(toFileList);

        Button myTraining = (Button) findViewById(R.id.my_training_button);
        myTraining.setOnClickListener(toTrainingList);

        Button database = (Button) findViewById(R.id.database_button);
        database.setOnClickListener(toDatabaseList);

        Button exit = (Button) findViewById(R.id.quit_button);
        exit.setOnClickListener(exitListener);

        bluetoothButton = (ImageView) findViewById(R.id.bluetooth_image_button);
        bluetoothButton.setOnClickListener(bluetoothListener);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if(isReceiver1)
        unregisterReceiver(bluetoothBroadcastReceiver);
        if(isReceiver2)
        unregisterReceiver(bluetoothDiscoverBroadcastReceiver);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        bluetoothButton.clearColorFilter();
    }

    private BluetoothService.SaveFileListener sfl = new BluetoothService.SaveFileListener() {
        @Override
        public void onSaveFile(String filename, String pgn) {
            String mydir = getApplicationContext().getDir("training", Context.MODE_PRIVATE).getPath();
            File file = new File(mydir, filename);
            FileOutputStream fos = null;

            try
            {
                fos = new FileOutputStream(file);
                fos.write(pgn.getBytes());
                fos.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

            runOnUiThread(new Thread(new Runnable() {
                @Override
                public void run() {
                    Toast t = Toast.makeText(getApplicationContext(),"New exercise available in Your Training!",Toast.LENGTH_SHORT);
                    t.show();
                }
            }));

            bluetoothImageNormal();
        }
    };

    private View.OnClickListener toChessEditor = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            int id = v.getId();
            if(id == R.id.new_game_button)
            {
                Intent intent = new Intent(getApplicationContext(), ChessEditor2.class);
                startActivityForResult(intent,300);
            }
            else if(id == R.id.edit_board_button)
            {
                Intent intent = new Intent(getApplicationContext(), ChessEditor.class);
                startActivityForResult(intent,301);
            }
        }
    };

    private View.OnClickListener toFileList = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(getApplicationContext(), ExercisesList.class);
            startActivity(intent);
        }
    };

    private View.OnClickListener toTrainingList = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(getApplicationContext(), TrainingList.class);
            startActivity(intent);
        }
    };

    private View.OnClickListener toDatabaseList = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getApplicationContext(), Database.class);
            startActivity(intent);
        }
    };

    private View.OnClickListener exitListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            exitDialog = new AlertDialog.Builder(MainScreen.this).create();
            exitDialog.setTitle("Exit application");
            exitDialog.setMessage("Are you sure you want to exit the application?");
            exitDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Exit",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    });
            exitDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                        }
                    });
            exitDialog.setCancelable(false);
            exitDialog.show();
        }
    };

    private View.OnClickListener bluetoothListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            enableBT();
        }
    };

    private final BroadcastReceiver bluetoothBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(bluetoothAdapter.ACTION_STATE_CHANGED))
            {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, bluetoothAdapter.ERROR);
                switch(state)
                {
                    case BluetoothAdapter.STATE_OFF:
                        bluetoothImageNormal();
                        Log.d("MainScreen","Bluetooth OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d("MainScreen","Bluetooth TURNING OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        visibleBT();
                        Log.d("MainScreen","Bluetooth ON");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d("MainScreen","Bluetooth TURNING ON");
                        break;
                }
            }
        }
    };

    private final BroadcastReceiver bluetoothDiscoverBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equals(bluetoothAdapter.ACTION_SCAN_MODE_CHANGED))
            {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

                switch(state)
                {
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                        startBluetoothConnectionService();
                        Log.d("MainScreen", "Device discoverable");
                        break;
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                        bluetoothImageShiny();
                        Log.d("MainScreen", "Device undiscoverable, can be connected to");
                        break;
                    case BluetoothAdapter.SCAN_MODE_NONE:
                        bluetoothImageShiny();
                        Log.d("MainScreen", "Device undiscoverable");
                        break;
                    case BluetoothAdapter.STATE_CONNECTING:
                        Log.d("MainScreen", "Device connecting");
                        break;
                    case BluetoothAdapter.STATE_CONNECTED:
                        Log.d("MainScreen", "Device connected");
                        break;
                }
            }
        }
    };

    private void enableBT()
    {
        if(bluetoothAdapter == null)
        {
            Log.e("MainActivity","No bluetooth on your device!");
            Toast.makeText(getApplicationContext(),"Can't find Bluetooth Module on your device",Toast.LENGTH_SHORT).show();
        }
        if(!bluetoothAdapter.isEnabled())
        {
            Intent btIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(btIntent);

            IntentFilter btIntentFilter = new IntentFilter(bluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(bluetoothBroadcastReceiver, btIntentFilter);
            isReceiver1 = true;
        }
        else
        {
            visibleBT();
        }
    }

    private void visibleBT()
    {
        Intent discoverIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 60);
        startActivity(discoverIntent);

        IntentFilter discoverIntentFilter = new IntentFilter(bluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        registerReceiver(bluetoothDiscoverBroadcastReceiver, discoverIntentFilter);
        isReceiver2 = true;
    }

    public void startBluetoothConnectionService()
    {
        btService = new BluetoothService(getApplicationContext(), true);
        btService.setSaveFileListener(sfl);
        bluetoothImageShiny();
        Toast.makeText(getApplicationContext(), "Ready to receive exercises",Toast.LENGTH_SHORT).show();
    }

    private void bluetoothImageShiny()
    {
        bluetoothButton.getDrawable().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        bluetoothButton.invalidate();
    }

    private void bluetoothImageNormal()
    {
        bluetoothButton.clearColorFilter();
    }

}
