package com.hetman.chesscoach.menus;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hetman.chesscoach.R;

import java.util.ArrayList;

/**
 * Created by shevson on 5/24/17.
 */

public class DiscoverableDevices extends ArrayAdapter<BluetoothDevice>
{
    private LayoutInflater layoutInflater;
    private ArrayList<BluetoothDevice> bluetoothDevices;
    private int viewResourceId;

    // Constructor
    public DiscoverableDevices (Context context, int resourceId, ArrayList<BluetoothDevice> devices)
    {
        super(context, resourceId, devices);
        this.bluetoothDevices = devices;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewResourceId = resourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = layoutInflater.inflate(viewResourceId, null);
        BluetoothDevice device = bluetoothDevices.get(position);

        if(device!=null)
        {
            TextView deviceName = (TextView) convertView.findViewById(R.id.device_name_text_view);
            TextView deviceAddress = (TextView) convertView.findViewById(R.id.device_address_text_view);

            if(deviceName != null)
            {
                deviceName.setText(device.getName());
            }
            if(deviceAddress != null)
            {
                deviceAddress.setText(device.getAddress());
            }
        }
        return convertView;
    }
}
