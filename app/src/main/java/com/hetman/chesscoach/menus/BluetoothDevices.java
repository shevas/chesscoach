package com.hetman.chesscoach.menus;

import android.app.DialogFragment;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hetman.chesscoach.R;

/**
 * Created by shevson on 5/24/17.
 */

public class BluetoothDevices extends DialogFragment
{
    static ArrayAdapter<BluetoothDevice> btAdapter;
    private IfBondedListener myListener;

    public BluetoothDevices()
    {
        // Nothing here
    }

    // Instatiation
    public static BluetoothDevices newInstance(ArrayAdapter<BluetoothDevice> adapter)
    {
        BluetoothDevices bd = new BluetoothDevices();
        btAdapter = adapter;
        return bd;
    }

    public interface IfBondedListener {
        void areBonded(BluetoothDevice device);
    }

    // Listener for bonded devices
    public void setMyListener(IfBondedListener listener)
    {
        this.myListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        this.getDialog().setCanceledOnTouchOutside(true);
        return inflater.inflate(R.layout.bluetooth_devices_fragment_layout, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstances)
    {
        super.onViewCreated(view, savedInstances);
        ListView devices = (ListView) view.findViewById(R.id.bluetooth_devices_list_view);
        devices.setAdapter(btAdapter);
        devices.setOnItemClickListener(makeBond);
    }

    // OnClick for selecting a device
    private AdapterView.OnItemClickListener makeBond = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            if(btAdapter.getItem(i).getBondState() == BluetoothDevice.BOND_BONDED)
            {
                myListener.areBonded(btAdapter.getItem(i));
            }
            else
            {
                btAdapter.getItem(i).createBond();
            }
            dismiss();
        }
    };
}
