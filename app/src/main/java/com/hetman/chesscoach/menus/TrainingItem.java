package com.hetman.chesscoach.menus;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hetman.chesscoach.ChessSolver;
import com.hetman.chesscoach.R;
import com.hetman.chesscoach.tools.PgnHelper;
import com.hetman.chesscoach.values.FileData;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by shevson on 5/21/17.
 */

public class TrainingItem extends ArrayAdapter<FileData>
{
    Context mContext;
    Activity myActivity;
    int layoutResourceId;
    ArrayList<FileData> dataArrayList = new ArrayList<>();
    String [] solved;

    // Constructor
    public TrainingItem(Activity myActivity, Context mContext, int layoutResourceId, ArrayList<FileData> dataArrayList, String [] solved)
    {
        super(mContext, layoutResourceId, dataArrayList);
        this.myActivity = myActivity;
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.dataArrayList = dataArrayList;
        this.solved = solved;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        TrainingItem.FileDataHolder fileDataHolder = null;

        if(row == null)
        {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row = inflater.inflate(layoutResourceId, parent, false);
            if(position%2!=0)
            {
                row.setBackgroundColor(mContext.getResources().getColor(R.color.mainBackground));
            }

            fileDataHolder = new FileDataHolder();
            fileDataHolder.textView = (TextView) row.findViewById(R.id.training_list_item_text);
            fileDataHolder.imageView = (ImageView) row.findViewById(R.id.training_item_image);
            fileDataHolder.solveButton = (Button) row.findViewById(R.id.list_training_solve);
            fileDataHolder.deleteButton = (Button) row.findViewById(R.id.list_training_delete);
            fileDataHolder.isSolved = (ImageView) row.findViewById(R.id.list_training_is_solved);
            row.setTag(fileDataHolder);
        }
        else
        {
            fileDataHolder = (FileDataHolder) row.getTag();
        }

        FileData dataObject = dataArrayList.get(position);
        fileDataHolder.textView.setText(dataObject.filename);
        switch (dataObject.level)
        {
            case "Amateur":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_pion);
                break;
            case "Novice":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_kon);
                break;
            case "Learner":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_goniec);
                break;
            case "Skilled":
                fileDataHolder.imageView.setImageResource(R.drawable.biala_wieza);
                break;
            case "Expert":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_hetman);
                break;
            default:
                break;
        }

        fileDataHolder.deleteButton.setOnClickListener(new DeleteButtonListener(dataObject.file, position));
        fileDataHolder.solveButton.setOnClickListener(new SolveButtonListener(dataObject.file));
        if(solved!=null)
        {
            for(int i=0; i<solved.length; i++)
            {
                if(solved[i].equals(dataObject.filename))
                {
                    fileDataHolder.isSolved.setImageResource(R.drawable.tick_icon);
                    fileDataHolder.solveButton.setText("Re-solve!");
                }
            }
        }
        return row;
    }

    static class FileDataHolder
    {
        TextView textView;
        ImageView imageView;
        ImageView isSolved;
        Button solveButton;
        Button deleteButton;
    }

    // OnClick for solve button
    private class SolveButtonListener implements View.OnClickListener
    {
        File file;
        public SolveButtonListener(File file)
        {
            this.file = file;
        }

        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(mContext, ChessSolver.class);
            intent.putExtra("pgnFile", PgnHelper.readPGNFile(file));
            intent.putExtra("filename", file.getName());
            myActivity.startActivityForResult(intent, 123);
        }
    }

    // OnClick for delete button
    private class DeleteButtonListener implements View.OnClickListener
    {
        File fileToDelete;
        int position;
        AlertDialog deleteAlert;
        public DeleteButtonListener(File fileToDelete, int position)
        {
            this.fileToDelete = fileToDelete;
            this.position = position;
        }

        @Override
        public void onClick(View view)
        {
            deleteAlert = new AlertDialog.Builder(myActivity).create();
            deleteAlert.setTitle("Delete training exercise");
            deleteAlert.setMessage("Are you sure you want to delete this exercise?");
            deleteAlert.setButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE, "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            deleteAlert.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    deleteFile(fileToDelete, position);
                }
            });
            deleteAlert.show();
        }
    }

    // Delete a file
    private void deleteFile(File fileToDelete, int position)
    {
        boolean wasDeleted = fileToDelete.delete();
        if(wasDeleted)
        {
            dataArrayList.remove(position);
            this.notifyDataSetChanged();
        }
    }
}
