package com.hetman.chesscoach.menus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hetman.chesscoach.R;
import com.hetman.chesscoach.values.DatabaseModel;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by shevson on 6/11/17.
 */

public class DatabaseItem extends ArrayAdapter<DatabaseModel>
{
    private Context mContext;
    private int layoutResourceId;
    private ArrayList<DatabaseModel> dataArrayList;

    // Constructor
    public DatabaseItem(Context mContext, int layoutResourceId, ArrayList<DatabaseModel> dataArrayList)
    {
        super(mContext, layoutResourceId,dataArrayList);
        this.mContext = mContext;
        this.layoutResourceId = layoutResourceId;
        this.dataArrayList = dataArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        DatabaseItem.FileDataHolder fileDataHolder = null;

        if(row==null)
        {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row = inflater.inflate(layoutResourceId, parent, false);
            if(position%2!=0)
            {
                row.setBackgroundColor(mContext.getResources().getColor(R.color.mainBackground));
            }

            fileDataHolder = new FileDataHolder();
            fileDataHolder.textView = (TextView) row.findViewById(R.id.database_list_text_view);
            fileDataHolder.imageView = (ImageView) row.findViewById(R.id.database_item_image);
            fileDataHolder.copyButton = (Button) row.findViewById(R.id.database_list_solve);
            row.setTag(fileDataHolder);
        }
        else
        {
            fileDataHolder = (DatabaseItem.FileDataHolder) row.getTag();
        }

        DatabaseModel dataModel = dataArrayList.get(position);
        fileDataHolder.textView.setText(dataModel.getName());
        fileDataHolder.copyButton.setOnClickListener(new AddToTrainingListener(dataModel));
        switch (dataModel.getLevel())
        {
            case "Amateur":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_pion);
                break;
            case "Novice":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_kon);
                break;
            case "Learner":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_goniec);
                break;
            case "Skilled":
                fileDataHolder.imageView.setImageResource(R.drawable.biala_wieza);
                break;
            case "Expert":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_hetman);
                break;
            default:
                break;
        }

        return row;
    }

    static class FileDataHolder
    {
        TextView textView;
        ImageView imageView;
        Button copyButton;
    }

    // Listener if user wants to add exercise to training list
    private class AddToTrainingListener implements View.OnClickListener
    {
        DatabaseModel model;
        public AddToTrainingListener(DatabaseModel model)
        {
            this.model = model;
        }
        @Override
        public void onClick(View v)
        {
            addToTraining(model);
        }
    }

    // Save exercise from database to Training
    private void addToTraining(DatabaseModel model)
    {
        String trdir = mContext.getDir("training", Context.MODE_PRIVATE).getPath();
        File file = new File(trdir, model.getName()+".pgn");
        FileOutputStream fos = null;
        StringBuilder stringBuilder = new StringBuilder();

        String startBracket = "[";
        String endBracket = "]\n";

        // Add OnMove
        stringBuilder.append(startBracket);
        stringBuilder.append("OnMove ");
        stringBuilder.append("\""+model.getOnMove()+"\"");
        stringBuilder.append(endBracket);

        // Add MoveNr
        stringBuilder.append(startBracket);
        stringBuilder.append("MoveNr ");
        stringBuilder.append("\""+model.getMoveNr()+"\"");
        stringBuilder.append(endBracket);

        // Add Question
        stringBuilder.append(startBracket);
        stringBuilder.append("Question ");
        stringBuilder.append("\""+model.getQuestion()+"\"");
        stringBuilder.append(endBracket);

        // Add Hint
        stringBuilder.append(startBracket);
        stringBuilder.append("Hint ");
        stringBuilder.append("\""+model.getHint()+"\"");
        stringBuilder.append(endBracket);

        // Add level
        stringBuilder.append(startBracket);
        stringBuilder.append("Level ");
        stringBuilder.append("\""+model.getLevel()+"\"");
        stringBuilder.append(endBracket);

        // Answer type
        stringBuilder.append(startBracket);
        stringBuilder.append("Answer ");
        stringBuilder.append("\""+model.getAnswer()+"\"");
        stringBuilder.append(endBracket);

        // Minutes and seconds
        stringBuilder.append(startBracket);
        stringBuilder.append("Timer ");
        stringBuilder.append("\""+model.getTimer()+"\"");
        stringBuilder.append(endBracket);

        // FEN position
        stringBuilder.append(startBracket);
        stringBuilder.append("FEN ");
        stringBuilder.append("\""+model.getFen()+"\"");
        stringBuilder.append(endBracket);

        // PGN
        stringBuilder.append(model.getPgn());

        String finalpgn = stringBuilder.toString();

        try {
            fos = new FileOutputStream(file);
            fos.write(finalpgn.getBytes());
            fos.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        Toast t = Toast.makeText(mContext,"Exercise successfully moved to your training!",Toast.LENGTH_SHORT);
        t.show();
    }
}
