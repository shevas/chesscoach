package com.hetman.chesscoach.menus;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hetman.chesscoach.ChessEditor;
import com.hetman.chesscoach.R;
import com.hetman.chesscoach.services.BluetoothService;
import com.hetman.chesscoach.tools.PgnHelper;
import com.hetman.chesscoach.values.FileData;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by shevson on 5/18/17.
 */

public class ExercisesItem extends ArrayAdapter<FileData>
{
    private static final UUID MY_UUID = UUID.fromString("26c39f0b-5ad2-48f1-9745-45c7589c12f8");

    Context mContext;
    Activity myActivity;
    int layoutResourceId;
    ArrayList<FileData> dataArrayList = new ArrayList<>();

    ViewGroup parentView;
    BluetoothAdapter bluetoothAdapter;
    File fileToSend;
    ArrayList<BluetoothDevice> btDevices = new ArrayList<>();
    DiscoverableDevices discoverableDevices;

    BluetoothService btService;
    BluetoothDevice deviceToConnect;
    Handler handler;

    ProgressDialog pd;
    ProgressDialog pd2;

    boolean br1 = false;
    boolean br2 = false;
    boolean br3 = false;

    int timer = 5;
    int timer2 = 10;
    boolean stayOn = true;

    // Constructor
    public ExercisesItem(Activity myActivity, Context mContext, int layoutResourceId, ArrayList<FileData> dataArrayList, BluetoothAdapter adapter)
    {
        super(mContext, layoutResourceId, dataArrayList);
        this.myActivity = myActivity;
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.dataArrayList = dataArrayList;
        bluetoothAdapter = adapter;
    }

    // Listener for bonded devices
    private BluetoothDevices.IfBondedListener myListener = new BluetoothDevices.IfBondedListener() {
        @Override
        public void areBonded(BluetoothDevice device) {
            deviceToConnect = device;
            bluetoothAdapter.cancelDiscovery();
            sendBTData();
        }
    };

    // Broadcast Receiver for Bluetooth Adapter State
    private final BroadcastReceiver bluetoothBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(BluetoothAdapter.ACTION_STATE_CHANGED))
            {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch(state)
                {
                    case BluetoothAdapter.STATE_OFF:
                        Log.d("EItem","Bluetooth OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d("EItem","Bluetooth TURNING OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        findDevices();
                        Log.d("EItem","Bluetooth ON");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d("EItem","Bluetooth TURNING ON");
                        break;
                }
            }
        }
    };

    // Broadcast Receiver for Bluetooth Device found
    private final BroadcastReceiver bluetoothDevicesBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equals(BluetoothDevice.ACTION_FOUND))
            {
                stayOn = false;
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                btDevices.add(device);
                discoverableDevices = new DiscoverableDevices(context, R.layout.list_bluetooth_devices, btDevices);
                FragmentManager fm = myActivity.getFragmentManager();
                BluetoothDevices bd = BluetoothDevices.newInstance(discoverableDevices);
                bd.setMyListener(myListener);
                bd.show(fm,"Bluetooth Devices");
            }
        }
    };

    // Broadcast Receiver for Bluetooth device bonded
    private final BroadcastReceiver bluetoothBondedBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED))
            {
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                switch(mDevice.getBondState())
                {
                    case BluetoothDevice.BOND_BONDED:
                        Log.d("EItem","Device "+mDevice.getName()+" bonded successfully");
                        bluetoothAdapter.cancelDiscovery();
                        deviceToConnect = mDevice;
                        sendBTData();
                        break;
                    case BluetoothDevice.BOND_BONDING:
                        Log.d("EItem","Device "+mDevice.getName()+" is bonding");
                        break;
                    case BluetoothDevice.BOND_NONE:
                        Log.d("EItem","No bond");
                        break;
                }
            }
        }
    };

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        parentView = parent;
        FileDataHolder fileDataHolder = null;

        if(row == null)
        {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row = inflater.inflate(layoutResourceId, parent, false);
            if(position%2!=0)
            {
                row.setBackgroundColor(mContext.getResources().getColor(R.color.mainBackground));
            }

            fileDataHolder = new FileDataHolder();
            fileDataHolder.textView = (TextView) row.findViewById(R.id.exercise_list_item_text);
            fileDataHolder.imageView = (ImageView) row.findViewById(R.id.exercise_item_image);
            fileDataHolder.deleteButton = (Button) row.findViewById(R.id.list_exercise_delete);
            fileDataHolder.editButton = (Button) row.findViewById(R.id.list_exercise_edit);
            fileDataHolder.sendButton = (Button) row.findViewById(R.id.list_exercise_solve);
            row.setTag(fileDataHolder);
        }
        else
        {
            fileDataHolder = (FileDataHolder) row.getTag();
        }

        FileData dataObject = dataArrayList.get(position);
        fileDataHolder.textView.setText(dataObject.filename);
        switch (dataObject.level)
        {
            case "Amateur":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_pion);
                break;
            case "Novice":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_kon);
                break;
            case "Learner":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_goniec);
                break;
            case "Skilled":
                fileDataHolder.imageView.setImageResource(R.drawable.biala_wieza);
                break;
            case "Expert":
                fileDataHolder.imageView.setImageResource(R.drawable.bialy_hetman);
                break;
            default:
                break;
        }
        fileDataHolder.deleteButton.setOnClickListener(new DeleteButtonListener(dataObject.file, position));
        fileDataHolder.editButton.setOnClickListener(new EditButtonListener(dataObject.file));
        fileDataHolder.sendButton.setOnClickListener(new SendButtonListener(dataObject.file));

        return row;
    }

    static class FileDataHolder
    {
        TextView textView;
        ImageView imageView;
        Button editButton;
        Button sendButton;
        Button deleteButton;
    }

    // OnClick for delete button
    private class DeleteButtonListener implements View.OnClickListener
    {
        File fileToDelete;
        int position;
        AlertDialog deleteAlert;
       public DeleteButtonListener(File fileToDelete, int position)
       {
          this.fileToDelete = fileToDelete;
          this.position = position;
       }

       @Override
        public void onClick(View view)
       {
           deleteAlert = new AlertDialog.Builder(myActivity).create();
           deleteAlert.setTitle("Delete exercise");
           deleteAlert.setMessage("Are you sure you want to delete this exercise?");
           deleteAlert.setButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE, "Cancel",
                   new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int which) {
                           dialog.dismiss();
                       }
                   });
           deleteAlert.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int which) {
                   deleteFile(fileToDelete, position);
               }
           });
           deleteAlert.show();
       }
    }

    private class EditButtonListener implements View.OnClickListener
    {
        File file;
        public EditButtonListener(File file)
        {
            this.file = file;
        }

        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(mContext, ChessEditor.class);
            intent.putExtra("pgnFile", PgnHelper.readPGNFile(file));
            intent.putExtra("filename", file.getName());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }
    }

    private class SendButtonListener implements View.OnClickListener
    {
        AlertDialog sendAlert;
        File file;
        public SendButtonListener(File file)
        {
            this.file = file;
        }
        @Override
        public void onClick(View v)
        {
            sendAlert = new AlertDialog.Builder(myActivity).create();
            sendAlert.setTitle("Send exercise");
            sendAlert.setMessage("Where do you wish to send the exercise to?");
            sendAlert.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, "Move to my training",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            moveFile(file);
                            Toast toast = Toast.makeText(mContext, "Exercised moved to your training!", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
            sendAlert.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "Send by Bluetooth", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if(!bluetoothAdapter.isEnabled())
                    {
                        fileToSend = file;
                        turnBTOn();
                    }
                    else
                    {
                        fileToSend = file;
                        findDevices();
                    }
                }
            });
            sendAlert.show();
        }
    }

    // Delete file from list
    private void deleteFile(File fileToDelete, int position)
    {
        boolean wasDeleted = fileToDelete.delete();
        if(wasDeleted)
        {
            dataArrayList.remove(position);
            this.notifyDataSetChanged();
        }
    }

    // Move file to training
    private void moveFile(File file)
    {
        String trdir = mContext.getDir("training", Context.MODE_PRIVATE).getPath();
        InputStream in = null;
        OutputStream out = null;
        File outputfile = new File(trdir, file.getName());
        try
        {
            in = new FileInputStream(file);
            out = new FileOutputStream(outputfile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            out.flush();
            out.close();
            out = null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    // Turn Bluetooth adapter on
    private void turnBTOn()
    {
        IntentFilter btIntentFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        myActivity.registerReceiver(bluetoothBroadcastReceiver, btIntentFilter);
        br1 = true;
        bluetoothAdapter.enable();
    }

    // Search for BT devices
    private void findDevices()
    {
        if(!bluetoothAdapter.isDiscovering())
        {
            checkForPermissions();
        }
        if(bluetoothAdapter.isDiscovering())
        {
            bluetoothAdapter.cancelDiscovery();
            checkForPermissions();
        }
        bluetoothAdapter.startDiscovery();
        IntentFilter discoverDevices = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        myActivity.registerReceiver(bluetoothDevicesBroadcastReceiver, discoverDevices);
        IntentFilter bondDevices = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        myActivity.registerReceiver(bluetoothBondedBroadcastReceiver, bondDevices);
        br2 = true;
        br3 = true;

        handler = new Handler();
        pd2 = new ProgressDialog(myActivity);
        pd2.setCancelable(false);
        pd2.setProgressStyle(android.R.style.Animation_Dialog);
        pd2.setMessage("Searching for bluetooth devices...");
        pd2.show();

        handler.postDelayed(waitforDevices,1000);
    }

    // Permission checker for different sdk version
    private void checkForPermissions()
    {
        if(Build.VERSION.SDK_INT >= 23)
        {
            getPermissions();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getPermissions()
    {
        int permissionCheck = myActivity.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
        permissionCheck += myActivity.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
        if(permissionCheck != 0)
        {
            myActivity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    // Sent data over BT
    private void sendBTData()
    {
        btService = new BluetoothService(mContext, false);
        btService.startClient(deviceToConnect, MY_UUID);

        pd = new ProgressDialog(myActivity);
        pd.setCancelable(false);
        pd.setProgressStyle(android.R.style.Animation_Dialog);
        pd.setMessage("Trying to send the exercise...");
        pd.show();

        handler = new Handler();
        handler.postDelayed(waitForSocketConnection, 1000);
    }

    private Runnable waitForSocketConnection = new Runnable() {
        @Override
        public void run() {
            if(btService.isConnected)
            {
                pd.dismiss();
                int size = (int) fileToSend.length();
                String name = fileToSend.getName();
                byte[] bytes = new byte[size];
                try
                {
                    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileToSend));
                    bis.read(bytes, 0, bytes.length);
                    bis.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                btService.write(name.getBytes());
                btService.write(bytes);
                pd.dismiss();
                Toast.makeText(mContext, "Exercise sent", Toast.LENGTH_SHORT).show();
            }
            else
            {
                if(timer==0)
                {
                    pd.dismiss();
                    timer = 5;
                    btService.stopClient();
                    Toast.makeText(mContext,"Cannot connect to the device",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    timer--;
                    handler.postDelayed(waitForSocketConnection,1000);
                }
            }
        }
    };

    private Runnable waitforDevices = new Runnable()
    {
        @Override
        public void run() {
            if(stayOn)
            {
                if(timer2==0)
                {
                    pd2.dismiss();
                    timer2 = 10;
                    Toast.makeText(mContext,"No devices found", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    timer2--;
                    handler.postDelayed(waitforDevices,1000);
                }
            }
            else
            {
                pd2.dismiss();
                stayOn = true;
            }
        }
    };

    // OnDestroy unregister all receivers
    public void unregisterAllReceivers()
    {
        if(br1)
        myActivity.unregisterReceiver(bluetoothBroadcastReceiver);
        if(br2)
        myActivity.unregisterReceiver(bluetoothDevicesBroadcastReceiver);
        if(br3)
        myActivity.unregisterReceiver(bluetoothBondedBroadcastReceiver);
    }
}
