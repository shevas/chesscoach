package com.hetman.chesscoach.menus;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.renderscript.ScriptGroup;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hetman.chesscoach.R;
import com.hetman.chesscoach.chessboard.ChessboardGame;
import com.hetman.chesscoach.tools.PgnHelper;
import com.hetman.chesscoach.values.pieces.ChessPiece;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by shevson on 5/2/17.
 */

public class AcceptEditor extends DialogFragment implements AdapterView.OnItemSelectedListener
{
    SavePositionEventListener myListener;

    Spinner colorSpinner;
    Spinner levelSpinner;
    RadioButton singleMove;
    RadioButton wholeVar;
    boolean isHidden = false;
    RelativeLayout mainFragment;
    View mContext;
    InputMethodManager imm;
    EditText filenameText;
    EditText onMoveText;
    EditText questionText;
    EditText hintText;
    NumberPicker minutes;
    NumberPicker seconds;
    ImageView levelImg;

    static Activity myAct;

    // Annotations
    String name = "";
    String onMove = "";
    String level = "";
    int moveNr = 0;
    String q = "";
    String h = "";
    boolean singleMoveFlag = false;
    boolean wholeVarFlag = false;
    int min = 0;
    int sec = 0;

    public AcceptEditor()
    {
        // Nothing here
    }

    // Instatiation
    public static AcceptEditor newInstance(String title, String annotationPGN, String filename, Activity myActivity)
    {
        AcceptEditor frag = new AcceptEditor();
        Bundle args = new Bundle();
        args.putString("title", title);
        if(annotationPGN!=null && filename!=null)
        {
            args.putString("annotationPGN", annotationPGN);
            args.putString("filename", filename);
        }
        myAct = myActivity;
        frag.setArguments(args);
        frag.setStyle(DialogFragment.STYLE_NORMAL, R.style.SaveEditorFragment);

        return frag;
    }

    // Event listener - callbacks
    public void setEventListener(AcceptEditor.SavePositionEventListener myListener)
    {
        this.myListener = myListener;
    }

    public interface SavePositionEventListener {
        boolean onEventOccured(String name, String onMove, int moveNr, String level, boolean singleMove, boolean wholeVar, String question, String hint, int minutes, int seconds);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        this.getDialog().setCanceledOnTouchOutside(false);
        return inflater.inflate(R.layout.accept_editor_fragment_layout, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        String oldPGN = getArguments().getString("annotationPGN");
        String filename = getArguments().getString("filename");

        mContext = this.getView();

        level = "Amateur";

        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        colorSpinner = (Spinner) view.findViewById(R.id.white_black_spinner);
        levelSpinner = (Spinner) view.findViewById(R.id.level_spinner);

        levelImg = (ImageView) view.findViewById(R.id.level_image);

        ArrayAdapter<CharSequence> leveladapter = new ArrayAdapter<CharSequence>(getActivity().getBaseContext(),R.layout.spinner_level_custom_layout, R.id.spinner_level_text_view, getResources().getStringArray(R.array.levels));
        leveladapter.setDropDownViewResource(R.layout.spinner_level_custom_layout);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getBaseContext(), R.array.colors, R.layout.spinner_item_custom_layout);
        adapter.setDropDownViewResource(R.layout.spinner_item_custom_layout);

        colorSpinner.setAdapter(adapter);
        colorSpinner.setOnItemSelectedListener(this);
        levelSpinner.setAdapter(leveladapter);
        levelSpinner.setOnItemSelectedListener(this);

        TextView show_hide = (TextView) view.findViewById(R.id.text_button_show_hide);
        show_hide.setOnClickListener(showhideButtonListener);

        minutes = (NumberPicker) view.findViewById(R.id.time_picker_min);
        minutes.setMinValue(0);
        minutes.setMaxValue(59);

        seconds = (NumberPicker) view.findViewById(R.id.time_picker_sec);
        seconds.setMinValue(0);
        seconds.setMaxValue(59);

        mainFragment = (RelativeLayout) view.findViewById(R.id.main_fragment_rel_layout);

        singleMove = (RadioButton) view.findViewById(R.id.radio_single);
        wholeVar = (RadioButton) view.findViewById(R.id.radio_whole);

        singleMove.setOnClickListener(onRadioButtonClicked);
        wholeVar.setOnClickListener(onRadioButtonClicked);

        Button cancelButton = (Button) view.findViewById(R.id.cancel_saving_button);
        cancelButton.setOnClickListener(onCancelButtonClicked);

        Button saveButton = (Button) view.findViewById(R.id.save_position_button);
        saveButton.setOnClickListener(onSaveButtonClicked);

        filenameText = (EditText) view.findViewById(R.id.exercise_name_edit_text);
        onMoveText = (EditText) view.findViewById(R.id.on_move_edit_text);
        questionText = (EditText) view.findViewById(R.id.question_edit_text);
        hintText = (EditText) view.findViewById(R.id.hint_edit_text);

        if(oldPGN!=null && filename!=null)
        {
            filenameText.setText(filename.substring(0,filename.lastIndexOf('.')));
            onMoveText.setText(PgnHelper.getMoveNrField(oldPGN));
            questionText.setText(PgnHelper.getQuestionField(oldPGN));
            hintText.setText(PgnHelper.getHintField(oldPGN));
            if(PgnHelper.getVariationField(oldPGN).equals("single"))
            {
                singleMove.setChecked(true);
                wholeVar.setChecked(false);
                singleMoveFlag = true;
                wholeVarFlag = false;
            }
            else
            {
                singleMove.setChecked(false);
                wholeVar.setChecked(true);
                singleMoveFlag = false;
                wholeVarFlag = true;
            }
            int pos = adapter.getPosition(PgnHelper.getOnMoveField(oldPGN));
            colorSpinner.setSelection(pos);

            int pos2 = leveladapter.getPosition(PgnHelper.getLevelField(oldPGN));
            levelSpinner.setSelection(pos2);

            String timer = PgnHelper.getTimeField(oldPGN);
            minutes.setValue(Integer.parseInt(timer.substring(0,timer.lastIndexOf(':'))));
            seconds.setValue(Integer.parseInt(timer.substring(timer.lastIndexOf(':')+1,timer.length())));
        }
    }

    // Selecting an item
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id)
    {
        if(view.getId() == R.id.spinner_text_view)
        {
            onMove = parent.getItemAtPosition(pos).toString();
        }
        else
        {
            level = parent.getItemAtPosition(pos).toString();
            switch(level)
            {
                case "Amateur":
                    levelImg.setImageResource(R.drawable.bialy_pion);
                    break;
                case "Novice":
                    levelImg.setImageResource(R.drawable.bialy_kon);
                    break;
                case "Learner":
                    levelImg.setImageResource(R.drawable.bialy_goniec);
                    break;
                case "Skilled":
                    levelImg.setImageResource(R.drawable.biala_wieza);
                    break;
                case "Expert":
                    levelImg.setImageResource(R.drawable.bialy_hetman);
                    break;
                default:
                    break;
            }
        }
    }

    public void onNothingSelected(AdapterView<?> parent)
    {
    }

    // OnClick for show/hide button
    private View.OnClickListener showhideButtonListener = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            final TextView me = (TextView) v;
            if(isHidden)
            {
                isHidden = false;
                me.setText("Hide");
                mainFragment.setVisibility(View.VISIBLE);
                mainFragment.setAlpha(1f);
            }
            else
            {
                isHidden = true;
                if(imm.isAcceptingText())
                {
                    imm.hideSoftInputFromWindow(mContext.getWindowToken(),0);
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        me.setText("Show");
                        mainFragment.setAlpha(0f);
                        mainFragment.setVisibility(View.INVISIBLE);}
                }, 300);
            }
        }
    };

    // OnClick for single move/whole variation choice
    private View.OnClickListener onRadioButtonClicked = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            switch(v.getId())
            {
                case R.id.radio_single:
                    if (singleMove.isChecked())
                    {
                        singleMoveFlag = true;
                        wholeVarFlag = false;
                        wholeVar.setChecked(false);
                    }
                    break;
                case R.id.radio_whole:
                    if (wholeVar.isChecked())
                    {
                        singleMoveFlag = false;
                        wholeVarFlag = true;
                        singleMove.setChecked(false);
                    }
                    break;
            }
        }
    };

    // OnClick for cancel button
    private View.OnClickListener onCancelButtonClicked = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            dismiss();
        }
    };

    // OnClick for save button
    private View.OnClickListener onSaveButtonClicked = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            name = filenameText.getText().toString();
            moveNr = Integer.parseInt(onMoveText.getText().toString());
            q = questionText.getText().toString();
            h = hintText.getText().toString();
            min = minutes.getValue();
            sec = seconds.getValue();
            if(myListener.onEventOccured(name,onMove, moveNr, level, singleMoveFlag, wholeVarFlag, q, h, min, sec))
            {
                Toast toast = Toast.makeText(mContext.getContext(), "Exercise saved!", Toast.LENGTH_SHORT);
                toast.show();
                dismiss();
                new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    myAct.finish();
                }
            },1000);
            }
            else
            {
                Toast toast = Toast.makeText(mContext.getContext(), "Wrong setup", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    };
}
