package com.hetman.chesscoach;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.hetman.chesscoach.chessboard.ChessboardEditor;
import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.chessboard.ChessboardGame;
import com.hetman.chesscoach.tools.PgnHelper;
import com.hetman.chesscoach.tools.ViewToBoardTools;
import com.hetman.chesscoach.values.pieces.ChessPiece;
import com.hetman.chesscoach.values.pieces.King;

public class ChessEditor extends AppCompatActivity {

    private ChessboardController chessboardController;
    private String pgnFile = "";
    private String filename = "";
    private ChessboardEditor cbgame;
    private ChessPiece [][] loadedPosition = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AttributeSet as = null;
        setContentView(R.layout.activity_chess_editor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.edit_board_toolbar);
        setSupportActionBar(toolbar);

        cbgame = (ChessboardEditor) findViewById(R.id.eb_chessboard);
        cbgame.setDrawListener(drawEndedListener);

        ImageView flipButton = (ImageView) findViewById(R.id.image_button);
        ImageView clearButton = (ImageView) findViewById(R.id.image_button2);
        ImageView setButton = (ImageView) findViewById(R.id.image_button3);
        flipButton.setOnTouchListener(imageTouchListener);
        clearButton.setOnTouchListener(imageTouchListener);
        setButton.setOnTouchListener(imageTouchListener);

        Button cancelButton = (Button) findViewById(R.id.eb_cancel);
        cancelButton.setOnClickListener(cancelButtonListener);

        Button nextButton = (Button) findViewById(R.id.eb_ok);
        nextButton.setOnClickListener(okButtonListener);

        if(getIntent().getStringExtra("pgnFile")!=null)
        {
            pgnFile = getIntent().getStringExtra("pgnFile");
            loadedPosition = PgnHelper.fenToPosition(PgnHelper.getFenField(pgnFile));
            chessboardController = new ChessboardController(PgnHelper.fenToPosition(PgnHelper.getFenField(pgnFile)));
        }
        else
        {
            chessboardController = new ChessboardController();
        }

        if(getIntent().getStringExtra("filename")!=null)
        {
            filename = getIntent().getStringExtra("filename");
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        finish();
    }

    private ChessboardGame.DidDrawListener drawEndedListener = new ChessboardGame.DidDrawListener() {
        @Override
        public boolean onDrawEnded() {
            setCustomPositionOnBoard();
            return false;
        }
    };

    private View.OnTouchListener imageTouchListener = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View v, MotionEvent event)
        {
            switch(event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                {
                    ImageView view = (ImageView) v;
                    view.getDrawable().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    view.invalidate();
                    break;
                }

                case MotionEvent.ACTION_UP:
                    int id = v.getId();
                    switch(id)
                    {
                        case R.id.image_button:
                        {
                            ChessboardEditor cbview = (ChessboardEditor) findViewById(R.id.eb_chessboard);
                            if(cbview.getFlipped())
                            {
                                cbview.setFlipped(false);
                            }
                            else
                            {
                                cbview.setFlipped(true);
                            }
                            break;
                        }
                        case R.id.image_button2:
                        {
                            ChessboardEditor cbview = (ChessboardEditor) findViewById(R.id.eb_chessboard);
                            cbview.clearChessBoard();
                            break;
                        }
                        case R.id.image_button3:
                        {
                            setInitialPositionOnBoard();
                            break;
                        }
                    }

                case MotionEvent.ACTION_CANCEL:
                {
                    ImageView view = (ImageView) v;
                    view.getDrawable().clearColorFilter();
                    view.invalidate();
                    break;
                }
            }
            return true;
        }
    };

    private View.OnClickListener cancelButtonListener = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            finish();
        }
    };

    private View.OnClickListener okButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            saveAndVerifyPosition();
        }
    };

    private void setInitialPositionOnBoard()
    {
        ChessboardEditor cbview = (ChessboardEditor) findViewById(R.id.eb_chessboard);
        cbview.clearChessBoard();
        chessboardController.setInitialPosition();
        for(int i=0; i<8; i++)
        {
            for(int k=0; k<8; k++)
            {
                ChessPiece piece = chessboardController.getSquare(i,k);
                if(piece!=null)
                {
                    cbview.setPiece(piece.getType(), piece.getColor(), String.valueOf(i), ViewToBoardTools.coordToString(k));
                }
            }
        }
        cbview.redraw();
    }

    private void setCustomPositionOnBoard()
    {
        ChessboardEditor cbview = (ChessboardEditor) findViewById(R.id.eb_chessboard);
        cbview.clearChessBoard();
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                ChessPiece piece = chessboardController.getSquare(i,j);
                if(piece!=null)
                {
                    cbview.setPiece(piece.getType(), piece.getColor(), String.valueOf(i), ViewToBoardTools.coordToString(j));
                }
            }
        }
        cbview.redraw();
    }

    private void saveAndVerifyPosition()
    {
        boolean samePosition = false;
        ChessboardEditor cbview = (ChessboardEditor) findViewById(R.id.eb_chessboard);
        ChessPiece [][] pieces = cbview.getPieces();

        if(chessboardController.verifyPosition(pieces))
        {
            if(loadedPosition!=null)
            {
                samePosition = true;
            }
            for(int i=0; i<8; i++)
            {
                for(int k=0; k<8; k++)
                {
                    chessboardController.setSquare(i,k,pieces[i][k]);
                    if(loadedPosition!=null)
                    {
                        if(pieces[i][k] != null && loadedPosition[i][k] != null)
                        {
                            if(pieces[i][k].getColor() != loadedPosition[i][k].getColor())
                            {
                                samePosition = false;
                            }
                            if(pieces[i][k].getType() != loadedPosition[i][k].getType())
                            {
                                samePosition = false;
                            }
                        }
                        else
                        {
                            if(pieces[i][k] == null && loadedPosition[i][k] == null)
                            {
                                // It's ok
                            }
                            else
                            {
                                samePosition = false;
                            }
                        }
                    }
                }
            }
            if(samePosition)
            {
                nextStep(true);
            }
            else
            {
                nextStep(false);
            }
        }
        else
        {
            Toast toast = Toast.makeText(this.getApplicationContext(), "Invalid position", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void nextStep(boolean samePosition)
    {
        Intent intent = new Intent(this, ChessEditor2.class);
        ChessPiece [] position = new ChessPiece[64];
        int iter = 0;
        for(int i=0; i<8; i++)
        {
            for(int k=0; k<8; k++)
            {
                position[iter] = chessboardController.getSquare(i,k);
                iter++;
            }
        }
        intent.putExtra("position", position);
        if(samePosition)
        {
            intent.putExtra("annotation", PgnHelper.getAnnotationsField(pgnFile));
        }
        if(!filename.equals(""))
        {
            intent.putExtra("filename", filename);
            intent.putExtra("fields", PgnHelper.getAllFields(pgnFile));
        }
        startActivityForResult(intent,401);
    }
}