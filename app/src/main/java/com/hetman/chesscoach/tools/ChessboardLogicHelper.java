package com.hetman.chesscoach.tools;

import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.values.ChessPieces;
import com.hetman.chesscoach.values.pieces.Bishop;
import com.hetman.chesscoach.values.pieces.ChessPiece;
import com.hetman.chesscoach.values.pieces.King;
import com.hetman.chesscoach.values.pieces.Knight;
import com.hetman.chesscoach.values.pieces.Pawn;
import com.hetman.chesscoach.values.pieces.Queen;
import com.hetman.chesscoach.values.pieces.Rook;

/**
 * Created by shevson on 4/29/17.
 */

public class ChessboardLogicHelper
{
    // Get all attacks from pieces like Rook, Bishop, Queen
    public static void getDiagLineAttacks (int x, int y, int xx, int yy, ChessboardController cc, boolean [][] attacked_squares)
    {
        int myx = x;
        int myy = y;
        boolean open = true;
        while(open)
        {
            myx += xx;
            myy += yy;
            if(myx<8 && myx>=0 && myy<8 && myy>=0)
            {
                attacked_squares[myx][myy] = true;
                if(cc.getSquare(myx,myy)!=null)
                {
                    open = false;
                }
            }
            else
            {
                open = false;
            }
        }
    }

    // Debugging method - prints out current position in simple code
    public static void printChessboard(ChessPiece[][] position)
    {
        for(int i=7; i>=0; i--)
        {
            for(int j=0; j<8; j++)
            {
                if(position[i][j]==null)
                {
                    System.out.print("[ ]");
                }
                else
                {
                    ChessPieces type = position[i][j].getType();
                    switch(type)
                    {
                        case PAWN: System.out.print("[p]"); break;
                        case KNIGHT: System.out.print("[N]"); break;
                        case BISHOP: System.out.print("[B]"); break;
                        case ROOK: System.out.print("[R]"); break;
                        case QUEEN: System.out.print("[Q]"); break;
                        case KING: System.out.print("[K]"); break;
                        default: System.out.print("[*]"); break;
                    }
                }
                if(j==7)
                {
                    System.out.print("\n");
                }
            }
        }
    }
}
