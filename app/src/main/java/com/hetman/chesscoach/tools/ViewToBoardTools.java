package com.hetman.chesscoach.tools;

import com.hetman.chesscoach.values.ChessPieces;
import com.hetman.chesscoach.values.pieces.Bishop;
import com.hetman.chesscoach.values.pieces.ChessPiece;
import com.hetman.chesscoach.values.pieces.King;
import com.hetman.chesscoach.values.pieces.Knight;
import com.hetman.chesscoach.values.pieces.Pawn;
import com.hetman.chesscoach.values.pieces.Queen;
import com.hetman.chesscoach.values.pieces.Rook;

/**
 * Created by shevson on 4/15/17.
 */

public class ViewToBoardTools
{
    public static String coordToString (int x)
    {
        String square = "";
        switch (x)
        {
            case 0: square = "a"; break;
            case 1: square = "b"; break;
            case 2: square = "c"; break;
            case 3: square = "d"; break;
            case 4: square = "e"; break;
            case 5: square = "f"; break;
            case 6: square = "g"; break;
            case 7: square = "h"; break;
            default: break;
        }
        return square;
    }

    public static int stringToCoord (String x)
    {
        int square;
        switch(x)
        {
            case "a": square = 0; break;
            case "b": square = 1; break;
            case "c": square = 2; break;
            case "d": square = 3; break;
            case "e": square = 4; break;
            case "f": square = 5; break;
            case "g": square = 6; break;
            case "h": square = 7; break;
            default: square = -1; break;
        }
        return square;
    }

    public static ChessPiece typeToPiece (ChessPieces type, boolean color)
    {
        ChessPiece cp;
        switch(type)
        {
            case PAWN: cp = new Pawn(color); break;
            case KNIGHT: cp = new Knight(color); break;
            case BISHOP: cp = new Bishop(color); break;
            case ROOK: cp = new Rook(color); break;
            case QUEEN: cp = new Queen(color); break;
            case KING: cp = new King(color); break;
            default: cp = null; break;
        }
        return cp;
    }
}
