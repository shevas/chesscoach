package com.hetman.chesscoach.tools;

import com.hetman.chesscoach.values.pieces.Bishop;
import com.hetman.chesscoach.values.pieces.ChessPiece;
import com.hetman.chesscoach.values.pieces.King;
import com.hetman.chesscoach.values.pieces.Knight;
import com.hetman.chesscoach.values.pieces.Pawn;
import com.hetman.chesscoach.values.pieces.Queen;
import com.hetman.chesscoach.values.pieces.Rook;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by shevson on 5/3/17.
 */

public class PgnHelper
{
    public static String positionToFEN(ChessPiece [][] position)
    {
        int emptyCounter = 0;
        String fenPosition = "";
        StringBuilder fenBuilder = new StringBuilder();

        for(int i=7; i>=0; i--)
        {
            for(int j=0; j<8; j++)
            {
                if(position[i][j]!=null)
                {
                    if(emptyCounter>0)
                    {
                        fenBuilder.append(emptyCounter);
                        emptyCounter = 0;
                    }
                    if(position[i][j].getColor())
                    {
                        switch(position[i][j].getType())
                        {
                            case PAWN: fenBuilder.append("P"); break;
                            case KNIGHT: fenBuilder.append("N"); break;
                            case BISHOP: fenBuilder.append("B"); break;
                            case ROOK: fenBuilder.append("R"); break;
                            case QUEEN: fenBuilder.append("Q"); break;
                            case KING: fenBuilder.append("K"); break;
                            default: break;
                        }
                    }
                    else
                    {
                        switch(position[i][j].getType())
                        {
                            case PAWN: fenBuilder.append("p"); break;
                            case KNIGHT: fenBuilder.append("n"); break;
                            case BISHOP: fenBuilder.append("b"); break;
                            case ROOK: fenBuilder.append("r"); break;
                            case QUEEN: fenBuilder.append("q"); break;
                            case KING: fenBuilder.append("k"); break;
                            default: break;
                        }
                    }
                }
                else
                {
                    emptyCounter++;
                }

                if(j+1==8)
                {
                    if(emptyCounter>0)
                    {
                        fenBuilder.append(emptyCounter);
                        emptyCounter = 0;
                    }
                    if(i-1>=0)
                    {
                        fenBuilder.append("/");
                    }
                }
            }
        }
        fenPosition = fenBuilder.toString();
        return fenPosition;
    }

    public static ChessPiece[][] fenToPosition(String fen)
    {
        int pos = 0;
        boolean stayAtChar = false;
        ChessPiece [][] position = new ChessPiece[8][8];
        for(int i=7; i>=0; i--)
        {
            for(int j=0; j<8; j++)
            {
                char p = fen.charAt(pos);
                String pString = String.valueOf(p);
                ChessPiece cp = null;
                switch(p)
                {
                    case 'P':
                        cp = new Pawn(true); break;
                    case 'N':
                        cp = new Knight(true); break;
                    case 'B':
                        cp = new Bishop(true); break;
                    case 'R':
                        cp = new Rook(true); break;
                    case 'Q':
                        cp = new Queen(true); break;
                    case 'K':
                        cp = new King(true); break;
                    case 'p':
                        cp = new Pawn(false); break;
                    case 'n':
                        cp = new Knight(false); break;
                    case 'b':
                        cp = new Bishop(false); break;
                    case 'r':
                        cp = new Rook(false); break;
                    case 'q':
                        cp = new Queen(false); break;
                    case 'k':
                        cp = new King(false); break;
                    default:
                        j = j + Integer.valueOf(pString)-1;
                        break;
                }
                pos++;
                position[i][j] = cp;
            }
            pos++;
        }
        return position;
    }


    public static String readPGNFile(File file)
    {
        String fullPgn = "";
        StringBuilder sb = new StringBuilder();
        BufferedReader br;
        try
        {
            br = new BufferedReader(new FileReader(file));
            String line;
            while((line = br.readLine()) !=null)
            {
                sb.append(line);
                sb.append('\n');
            }
            br.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        fullPgn = sb.toString();

        return fullPgn;
    }

    public static String getFenField(String pgnString)
    {
        String fen = "";
        int i = pgnString.indexOf("[FEN \"")+6;
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            if(c=='\"')
            {
                break;
            }
            fen = fen + c;
        }
        return fen;
    }

    public static String getAnnotationsField(String pgnString)
    {
        String annotation = "";
        int i = pgnString.indexOf("1. ");
        if(i == -1)
        {
            i = pgnString.indexOf("1... ");
        }
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            annotation = annotation + c;
        }
        return annotation;
    }

    public static String getAllFields(String pgnString)
    {
        String fields = "";
        int i = pgnString.indexOf("1. ");
        if(i == -1)
        {
            i = pgnString.indexOf("1... ");
        }
        for(int k=0; k<i; k++)
        {
            char c = pgnString.charAt(k);
            fields = fields + c;
        }
        return fields;
    }

    public static String getOnMoveField(String pgnString)
    {
        String onMove = "";
        int i = pgnString.indexOf("[OnMove \"")+9;
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            if(c=='\"')
            {
                break;
            }
            onMove = onMove + c;
        }
        return onMove;
    }

    public static String getMoveNrField(String pgnString)
    {
        String moveNr = "";
        int i = pgnString.indexOf("[MoveNr \"")+9;
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            if(c=='\"')
            {
                break;
            }
            moveNr = moveNr + c;
        }
        return moveNr;
    }

    public static String getQuestionField(String pgnString)
    {
        String question = "";
        int i = pgnString.indexOf("[Question \"")+11;
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            if(c=='\"')
            {
                break;
            }
            question = question + c;
        }
        return question;
    }

    public static String getHintField(String pgnString)
    {
        String hint = "";
        int i = pgnString.indexOf("[Hint \"")+7;
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            if(c=='\"')
            {
                break;
            }
            hint = hint + c;
        }
        return hint;
    }

    public static String getVariationField(String pgnString)
    {
        String variation = "";
        int i = pgnString.indexOf("[Answer \"")+9;
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            if(c=='\"')
            {
                break;
            }
            variation = variation + c;
        }
        return variation;
    }

    public static String getLevelField(String pgnString)
    {
        String level = "";
        int i = pgnString.indexOf("[Level \"")+8;
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            if(c=='\"')
            {
                break;
            }
            level = level + c;
        }
        return level;
    }

    public static String getTimeField(String pgnString)
    {
        String time = "";
        int i = pgnString.indexOf("[Timer \"")+8;
        for(int k=i; k<pgnString.length(); k++)
        {
            char c = pgnString.charAt(k);
            if(c=='\"')
            {
                break;
            }
            time = time + c;
        }
        return time;
    }

    public static ChessPiece getPieceFromNotation(String move, boolean color)
    {
        ChessPiece cp = null;
        if(move.equals("0-0") || move.equals("0-0-0"))
        {
            cp = new King(color);
        }
        else
        {
            char c = move.charAt(0);
            switch (c)
            {
                case 'S':
                    cp = new Knight(color);
                    break;
                case 'G':
                    cp = new Bishop(color);
                    break;
                case 'W':
                    cp = new Rook(color);
                    break;
                case 'H':
                    cp = new Queen(color);
                    break;
                case 'K':
                    cp = new King(color);
                    break;
                default:
                    cp = new Pawn(color);
                    break;
            }
        }
        return cp;
    }

    public static String getMoveFromNotation(String move)
    {
        String finalMove = "";

        move = move.replaceAll("\\+","");
        move = move.replaceAll("\\#","");
        move = move.replaceAll("\n","");

        if(move.equals("0-0-0") || move.equals("0-0"))
        {
            finalMove = move;
        }
        else
        {
            // Pawn moves
            if(move.charAt(0) == move.toLowerCase().charAt(0))
            {
                // Taking
                if(move.charAt(1)=='x')
                {
                    finalMove = String.valueOf(move.charAt(0))+String.valueOf(move.charAt(2)+String.valueOf(move.charAt(3)));
                }
                // Normal move
                else
                {
                    finalMove = String.valueOf(move.charAt(0))+String.valueOf(move.charAt(1));
                }
            }
            else
            {
                move = move.replaceAll("x","");
                if(move.length()>3)
                {
                    finalMove = String.valueOf(move.charAt(1))+String.valueOf(move.charAt(2)+String.valueOf(move.charAt(3)));
                }
                else
                {
                    finalMove = String.valueOf(move.charAt(1))+String.valueOf(move.charAt(2));
                }
            }
        }
        return finalMove;
    }
}
