package com.hetman.chesscoach;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.text.Annotation;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hetman.chesscoach.annotations.Annotations;
import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.chessboard.ChessboardEditor;
import com.hetman.chesscoach.chessboard.ChessboardGame;
import com.hetman.chesscoach.menus.AcceptEditor;
import com.hetman.chesscoach.tools.PgnHelper;
import com.hetman.chesscoach.tools.ViewToBoardTools;
import com.hetman.chesscoach.values.ChessPieces;
import com.hetman.chesscoach.values.pieces.Bishop;
import com.hetman.chesscoach.values.pieces.ChessPiece;
import com.hetman.chesscoach.values.pieces.King;
import com.hetman.chesscoach.values.pieces.Knight;
import com.hetman.chesscoach.values.pieces.Pawn;
import com.hetman.chesscoach.values.pieces.Queen;
import com.hetman.chesscoach.values.pieces.Rook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChessEditor2 extends AppCompatActivity {

    ChessboardController chessboardController;
    ChessPiece [][] position;
    ChessboardGame cbgame;
    Annotations gameAnnotation;
    Dialog promoDialog;
    AlertDialog overrideDialog;
    Button saveButton;
    Context mContext;
    boolean fragmentShown = false;
    boolean startingPosition = false;
    String annotationPGN = null;
    String filename = null;
    String fieldsPGN = null;

    Activity myActivity;

    // Promotion
    String promotionfromx = "";
    String promotionfromy = "";
    String promotiontox = "";
    String promotiontoy = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chess_editor2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.play_board_toolbar);
        setSupportActionBar(toolbar);

        mContext = getApplicationContext();
        myActivity = this;

        cbgame = (ChessboardGame) findViewById(R.id.eb_chessboard2);
        cbgame.setEventListener(moveEventListener);
        cbgame.setDrawListener(drawEndedListener);

        Button cancelButton = (Button) findViewById(R.id.eb_back);
        cancelButton.setOnClickListener(cancelButtonListener);

        saveButton = (Button) findViewById(R.id.eb_ok2);
        saveButton.setOnClickListener(nextButtonListener);

        ImageView flipButton = (ImageView) findViewById(R.id.image_button_2);
        ImageView backButton = (ImageView) findViewById(R.id.image_button_22);
        ImageView forwardButton = (ImageView) findViewById(R.id.image_button_23);
        flipButton.setOnTouchListener(imageTouchListener);
        backButton.setOnTouchListener(imageTouchListener);
        forwardButton.setOnTouchListener(imageTouchListener);

        TextView gameText = (TextView) findViewById(R.id.chess_notation_text);
        gameAnnotation = new Annotations(gameText);

        promoDialog = new Dialog(this);
        promoDialog.setCancelable(false);
        promoDialog.setContentView(R.layout.promotion_dialog);

        overrideDialog = new AlertDialog.Builder(this).create();
        overrideDialog.setTitle("Override move");
        overrideDialog.setMessage("Do you wish to override old move with the new move?");

        Parcelable[] parcelables;
        if(getIntent().getParcelableArrayExtra("position")!=null)
        {
            parcelables = getIntent().getParcelableArrayExtra("position");
            position = new ChessPiece[8][8];
            int iter = 0;
            for(int i=0; i<8; i++)
            {
                for(int k=0; k<8; k++)
                {
                    if(parcelables[iter] instanceof King)
                    {
                        position[i][k] = (King) parcelables[iter];
                    }
                    else if(parcelables[iter] instanceof Queen)
                    {
                        position[i][k] = (Queen) parcelables[iter];
                    }
                    else if(parcelables[iter] instanceof Rook)
                    {
                        position[i][k] = (Rook) parcelables[iter];
                    }
                    else if(parcelables[iter] instanceof Bishop)
                    {
                        position[i][k] = (Bishop) parcelables[iter];
                    }
                    else if(parcelables[iter] instanceof Knight)
                    {
                        position[i][k] = (Knight) parcelables[iter];
                    }
                    else if(parcelables[iter] instanceof Pawn)
                    {
                        position[i][k] = (Pawn) parcelables[iter];
                    }
                    iter++;
                }
            }
        }
        else
        {
            startingPosition = true;
        }

        if(getIntent().getStringExtra("annotation")!=null)
        {
            annotationPGN = getIntent().getStringExtra("annotation");
        }
        if(getIntent().getStringExtra("filename")!=null)
        {
            filename = getIntent().getStringExtra("filename");
        }
        if(getIntent().getStringExtra("fields")!=null)
        {
            fieldsPGN = getIntent().getStringExtra("fields");
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    private View.OnClickListener cancelButtonListener = new View.OnClickListener(){
        @Override
        public void onClick(View v)
        {
            finish();
        }
    };

    private View.OnClickListener nextButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            FragmentManager fm = getFragmentManager();
            AcceptEditor ae = AcceptEditor.newInstance("Setup your exercise", fieldsPGN, filename, myActivity);
            ae.setEventListener(savePositionEventListener);
            ae.show(fm, "Finish editor");
        }
    };

    private AcceptEditor.SavePositionEventListener savePositionEventListener = new AcceptEditor.SavePositionEventListener() {
        @Override
        public boolean onEventOccured(String name, String onMove, int moveNr, String level, boolean singleMove, boolean wholeVar, String question, String hint, int minutes, int seconds)
        {
            return gameAnnotation.saveAnnotationsToPGN(name, onMove, moveNr, level, singleMove, wholeVar, question, hint, minutes, seconds, mContext);
        }
    };

    private ChessboardGame.DidDrawListener drawEndedListener = new ChessboardGame.DidDrawListener() {
        @Override
        public boolean onDrawEnded() {
            setSavedPosition(position);
            return false;
        }
    };

    private ChessboardGame.PieceToMoveEventListener moveEventListener = new ChessboardGame.PieceToMoveEventListener() {
        @Override
        public void onEventOccured(ChessPiece cp, String xfrom, String yfrom, String xto, String yto)
        {
            if(!chessboardController.isOnMoveSet())
            {
                chessboardController.setOnMove(cp.getColor());
            }

            if(chessboardController.makeMove(cp, Integer.parseInt(xfrom), ViewToBoardTools.stringToCoord(yfrom), Integer.parseInt(xto), ViewToBoardTools.stringToCoord(yto)))
            {
                // Check if pawn is promoting
                if(chessboardController.isPromoting())
                {
                    promoDialog.show();
                    ImageView hetman = (ImageView) promoDialog.findViewById(R.id.promocja_hetman);
                    ImageView wieza = (ImageView) promoDialog.findViewById(R.id.promocja_wieza);
                    ImageView goniec = (ImageView) promoDialog.findViewById(R.id.promocja_goniec);
                    ImageView skoczek = (ImageView) promoDialog.findViewById(R.id.promocja_kon);
                    if(!chessboardController.getPromoColour())
                    {
                        hetman.setImageResource(R.drawable.czarny_hetman);
                        wieza.setImageResource(R.drawable.czarna_wieza);
                        goniec.setImageResource(R.drawable.czarny_goniec);
                        skoczek.setImageResource(R.drawable.czarny_kon);
                    }
                    hetman.setOnClickListener(new PromoteListener(ChessPieces.QUEEN,cp, xfrom, yfrom, xto, yto));
                    wieza.setOnClickListener(new PromoteListener(ChessPieces.ROOK,cp, xfrom, yfrom, xto, yto));
                    goniec.setOnClickListener(new PromoteListener(ChessPieces.BISHOP,cp, xfrom, yfrom, xto, yto));
                    skoczek.setOnClickListener(new PromoteListener(ChessPieces.KNIGHT,cp, xfrom, yfrom, xto, yto));

                    promotionfromx = xfrom;
                    promotionfromy = yfrom;
                    promotiontox = xto;
                    promotiontoy = yto;

                }
                else
                {
                    final boolean moveNow = chessboardController.getOnMove();
                    if(gameAnnotation.checkIfMoveExists(chessboardController.getOnMove()))
                    {
                        overrideDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        try
                                        {
                                            chessboardController = (ChessboardController) gameAnnotation.ccOnCurrentMove(moveNow).clone();
                                        }
                                        catch (CloneNotSupportedException e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        overrideDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                new OverrideListener(cp, xfrom, yfrom, xto, yto));
                        overrideDialog.setCancelable(false);
                        overrideDialog.show();
                    }
                    else
                    {
                        setNewPosition();
                        addAnnotation(cp, xfrom, yfrom, xto, yto);
                    }
                }
            }
            else
            {
                // Invalid move!
            }
        }
    };

    private class PromoteListener implements View.OnClickListener
    {
        ChessPieces mypiece;
        ChessPiece cp;
        String xfrom;
        String yfrom;
        String xto;
        String yto;
        public PromoteListener(ChessPieces piece, ChessPiece cp, String xfrom, String yfrom, String xto, String yto)
        {
            mypiece = piece;
            this.cp = cp;
            this.xfrom = xfrom;
            this.yfrom = yfrom;
            this.xto = xto;
            this.yto = yto;
        }

        @Override
        public void onClick(View v)
        {
            chessboardController.promote(mypiece);
            final boolean moveNow = chessboardController.getOnMove();
            if(gameAnnotation.checkIfMoveExists(chessboardController.getOnMove()))
            {
                overrideDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                try
                                {
                                    chessboardController = (ChessboardController) gameAnnotation.ccOnCurrentMove(moveNow).clone();
                                }
                                catch (CloneNotSupportedException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });
                overrideDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new OverrideListener(cp, xfrom, yfrom, xto, yto));
                overrideDialog.setCancelable(false);
                overrideDialog.show();
            }
            else
            {
                setNewPosition();
                addAnnotation(cp, xfrom, yfrom, xto, yto);
            }
            promoDialog.dismiss();
        }
    }

    private class OverrideListener implements DialogInterface.OnClickListener
    {
        ChessPiece mycp;
        String xfrom, yfrom, xto, yto;

        public OverrideListener(ChessPiece cp, String xfrom, String yfrom, String xto, String yto)
        {
            this.mycp = cp;
            this.xfrom = xfrom;
            this.yfrom = yfrom;
            this.xto = xto;
            this.yto = yto;
        }
        @Override
        public void onClick(DialogInterface dialog, int which)
        {
            addAnnotation(mycp, xfrom, yfrom, xto, yto);
            setNewPosition();
        }
    }

    private View.OnTouchListener imageTouchListener = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View v, MotionEvent event)
        {
            switch(event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                {
                    ImageView view = (ImageView) v;
                    view.getDrawable().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    view.invalidate();
                    break;
                }

                case MotionEvent.ACTION_UP:
                    int id = v.getId();
                    switch(id)
                    {
                        case R.id.image_button_2:
                        {
                            if(cbgame.getFlipped())
                            {
                                cbgame.setFlipped(false);
                            }
                            else
                            {
                                cbgame.setFlipped(true);
                            }
                            break;
                        }
                        case R.id.image_button_22:
                        {
                            ChessboardController newcc = null;
                            newcc = gameAnnotation.moveForward(chessboardController.getOnMove());
                            if(newcc!=null)
                            {
                                try
                                {
                                    chessboardController = (ChessboardController) newcc.clone();
                                }
                                catch (CloneNotSupportedException e)
                                {
                                    e.printStackTrace();
                                }
                                setNewPosition();
                            }
                            break;
                        }
                        case R.id.image_button_23:
                        {
                            ChessboardController newcc = null;
                            newcc = gameAnnotation.moveBack(chessboardController.getOnMove());
                            if(newcc!=null)
                            {
                                try
                                {
                                    chessboardController = (ChessboardController) newcc.clone();
                                }
                                catch (CloneNotSupportedException e)
                                {
                                    e.printStackTrace();
                                }
                                setNewPosition();
                            }
                            break;
                        }
                    }

                case MotionEvent.ACTION_CANCEL:
                {
                    ImageView view = (ImageView) v;
                    view.getDrawable().clearColorFilter();
                    view.invalidate();
                    break;
                }
            }
            return true;
        }
    };

    // Standard Add Annotation (when inserting new moves)
    private void addAnnotation(ChessPiece cp, String xfrom, String yfrom, String xto, String yto)
    {
        if(chessboardController.getSameCoordinate().equals(""))
        {
            gameAnnotation.addMove(chessboardController,cp, xfrom, yfrom, xto, yto, chessboardController.isTaking(), chessboardController.isChecking(), chessboardController.wasCastled(),chessboardController.getOnMove(), chessboardController.wasMated(), null, chessboardController.getSameCoordinate());
        }
        else
        {
            gameAnnotation.addMove(chessboardController,cp, xfrom, yfrom, xto, yto, chessboardController.isTaking(), chessboardController.isChecking(), chessboardController.wasCastled(),chessboardController.getOnMove(), chessboardController.wasMated(), null, chessboardController.getSameCoordinate());
        }
    }

    // Set saved position on Chessboard
    private void setSavedPosition(ChessPiece [][] position)
    {
        ChessboardGame cbview = (ChessboardGame) findViewById(R.id.eb_chessboard2);

        if(startingPosition)
        {
            chessboardController = new ChessboardController();
            chessboardController.setInitialPosition();
        }
        else
        {
            chessboardController = new ChessboardController(position);
        }
        for(int i=0; i<8; i++)
        {
            for(int k=0; k<8; k++)
            {
                ChessPiece piece = chessboardController.getSquare(i,k);
                if(piece!=null)
                {
                    cbview.setPiece(piece.getType(), piece.getColor(), String.valueOf(i), ViewToBoardTools.coordToString(k));
                }
            }
        }
        gameAnnotation.addInitialPosition(chessboardController);
        if(annotationPGN!=null)
        {
            setInitialAnnotation();
        }

        cbview.redraw();
    }

    // Set a new position on Chessboard
    private void setNewPosition()
    {
        ChessPiece [][] position = new ChessPiece[8][8];
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                position[i][j] = chessboardController.getSquare(i,j);
            }
        }
        cbgame.setPieces(position);
    }

    // Set annotation from PGN file
    private void setInitialAnnotation()
    {
        String currentGameText = annotationPGN;
        int moveCounter = 1;
        boolean nowMoving = true;
        boolean nowFirst = true;
        boolean moveChanger = true;
        int totalMoves = 0;
        int phaseCounter = 0;
        int start = 0;
        String [] movePhase = {"moveNr", "moveW", "moveB"};
        boolean moveValid = false;

        ArrayList<Integer> moves = new ArrayList<>();
        Map<Integer, String> moveTexts = new HashMap<>();
        Map<Integer, ChessboardController> movePositions = new HashMap<>();

        ChessboardController mycc = null;

        String themove = "";

        try
        {
            mycc = (ChessboardController) chessboardController.clone();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String [] movesSplit = currentGameText.split(" ");
        if(movesSplit[0].equals("1."))
        {
            nowMoving = true;
            nowFirst = true;
            moveCounter = 1;
            chessboardController.setOnMove(true);
            mycc.setOnMove(true);
        }
        else
        {
            nowMoving = false;
            nowFirst = false;
            moveCounter = -1;
            chessboardController.setOnMove(false);
            mycc.setOnMove(false);
            moveChanger = false;
        }
        for(int i=0; i<movesSplit.length; i++)
        {
            if(!movesSplit[i].equals("1-0") && !movesSplit[i].equals("0-1") && !movesSplit[i].equals("\n"))
            {
                themove = themove + movesSplit[i];
                if(movePhase[phaseCounter].equals("moveNr"))
                {
                    themove = themove + " ";
                    phaseCounter++;
                    if(!moveChanger)
                    {
                        phaseCounter++;
                        moveChanger = true;
                    }
                }
                else if(movePhase[phaseCounter].equals("moveW"))
                {
                    themove = themove + " ";

                    // Moves
                    moves.add(moveCounter);

                    // Move texts
                    moveTexts.put(moveCounter,themove);

                    // Move positions
                    ChessPiece chessPiece = PgnHelper.getPieceFromNotation(movesSplit[i],nowMoving);
                    String shortmove = PgnHelper.getMoveFromNotation(movesSplit[i]);
                    if(shortmove.equals("0-0"))
                    {
                        moveValid = mycc.makeMove(chessPiece, 0, 6, '-', nowMoving);
                    }
                    else if(shortmove.equals("0-0-0"))
                    {
                        moveValid = mycc.makeMove(chessPiece, 0, 2, '-', nowMoving);
                    }
                    else
                    {
                        if(shortmove.length()>2)
                        {
                            char cextra = shortmove.charAt(0);
                            char c1 = shortmove.charAt(1);
                            char c2 = shortmove.charAt(2);
                            moveValid = mycc.makeMove(chessPiece, Integer.parseInt(String.valueOf(c2))-1, ViewToBoardTools.stringToCoord(String.valueOf(c1)), cextra, nowMoving);

                        }
                        else
                        {
                            char c1 = shortmove.charAt(0);
                            char c2 = shortmove.charAt(1);
                            moveValid = mycc.makeMove(chessPiece,Integer.parseInt(String.valueOf(c2))-1,ViewToBoardTools.stringToCoord(String.valueOf(c1)),'-', nowMoving);
                        }
                    }

                    try
                    {
                        ChessboardController cc = (ChessboardController) mycc.clone();
                        movePositions.put(moveCounter,cc);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    moveCounter = moveCounter * (-1);
                    phaseCounter++;
                    themove = "";
                    nowMoving = false;
                    totalMoves++;
                }
                else
                {
                    themove = themove + " ";

                    // Moves
                    moves.add(moveCounter);

                    // Move texts
                    moveTexts.put(moveCounter,themove);

                    // Move positions
                    ChessPiece chessPiece = PgnHelper.getPieceFromNotation(movesSplit[i],nowMoving);
                    String shortmove = PgnHelper.getMoveFromNotation(movesSplit[i]);
                    if(shortmove.equals("0-0"))
                    {
                        moveValid = mycc.makeMove(chessPiece, 7, 6, 'g', nowMoving);
                    }
                    else if(shortmove.equals("0-0-0"))
                    {
                        moveValid = mycc.makeMove(chessPiece, 7, 2, 'c', nowMoving);
                    }
                    else
                    {
                        if(shortmove.length()>2)
                        {
                            char cextra = shortmove.charAt(0);
                            char c1 = shortmove.charAt(1);
                            char c2 = shortmove.charAt(2);
                            moveValid = mycc.makeMove(chessPiece,Integer.parseInt(String.valueOf(c2))-1,ViewToBoardTools.stringToCoord(String.valueOf(c1)),cextra, nowMoving);
                        }
                        else
                        {
                            char c1 = shortmove.charAt(0);
                            char c2 = shortmove.charAt(1);
                            moveValid = mycc.makeMove(chessPiece,Integer.parseInt(String.valueOf(c2))-1,ViewToBoardTools.stringToCoord(String.valueOf(c1)),'-', nowMoving);
                        }
                    }

                    try
                    {
                        ChessboardController cc = (ChessboardController) mycc.clone();
                        movePositions.put(moveCounter,cc);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    moveCounter = Math.abs(moveCounter) + 1;
                    phaseCounter = 0;
                    themove = "";
                    nowMoving = true;
                }
            }
            else
            {
                break;
            }
        }
        if(nowFirst)
        {
            gameAnnotation.setInitialAnnotation(currentGameText, moves, moveTexts, movePositions, 1, totalMoves);
        }
        else
        {
            gameAnnotation.setInitialAnnotation(currentGameText, moves, moveTexts, movePositions, -1, totalMoves);
        }
    }
}
