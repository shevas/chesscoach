package com.hetman.chesscoach;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.hetman.chesscoach.menus.ExercisesItem;
import com.hetman.chesscoach.values.FileData;

import java.io.File;
import java.util.ArrayList;

public class ExercisesList extends AppCompatActivity {

    private ListView lv;
    private BluetoothAdapter bluetoothAdapter;
    private ExercisesItem adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.exercises_toolbar);
        setSupportActionBar(toolbar);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        populateFileList(getApplicationContext());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if(adapter!=null)
        {
            adapter.unregisterAllReceivers();
        }
    }

    private void populateFileList(Context ctx)
    {
        ArrayList<FileData> data = new ArrayList<>();
        File path = ctx.getDir("exercises", Context.MODE_PRIVATE);

        File [] inPathFiles = path.listFiles();

        if(inPathFiles.length>0)
        {
            for(int i=0; i<inPathFiles.length; i++)
            {
                String filename = inPathFiles[i].getName().substring(0, inPathFiles[i].getName().lastIndexOf("."));
                data.add(new FileData(filename, inPathFiles[i]));
            }
            adapter = new ExercisesItem(ExercisesList.this, ctx, R.layout.list_item_exercises_list,data, bluetoothAdapter);
            lv = (ListView) findViewById(R.id.exercises_list);
            lv.setAdapter(adapter);
        }
    }

}
