package com.hetman.chesscoach.values.pieces;

import android.os.Parcel;
import android.os.Parcelable;

import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.tools.ChessboardLogicHelper;
import com.hetman.chesscoach.values.ChessPieces;

/**
 * Created by shevson on 4/15/17.
 */

public class Bishop extends ChessPiece
{
    public Bishop(boolean color)
    {
        super(color);
        this.type = ChessPieces.BISHOP;
    }

    public Bishop(Parcel in)
    {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeByte((byte) (color ? 1 : 0));
        dest.writeString(type.toString());
    }

    public static final Parcelable.Creator<Bishop> CREATOR = new Parcelable.Creator<Bishop>()
    {
        public Bishop createFromParcel(Parcel in)
        {
            return new Bishop(in) {
            };
        }

        public Bishop[] newArray(int size)
        {
            return new Bishop[size];
        }
    };

    @Override
    public boolean tryToMove(ChessboardController cc, int xfrom, int yfrom, int xto, int yto, boolean virtualMove)
    {
        boolean moveOK = false;
        attackedByMe(xfrom, yfrom, cc);
        if(attacked_squares[xto][yto])
        {
            if(cc.getSquare(xto, yto)==null)
            {
                moveOK = true;
            }
            else
            {
                if(cc.getSquare(xto, yto).getColor()!=this.color)
                {
                    moveOK = true;
                }
            }
        }

        if(moveOK && !virtualMove)
        {
            attackedByMe(xto, yto, cc);
        }
        return moveOK;
    }

    @Override
    public boolean [][] attackedByMe(int myX, int myY, ChessboardController cc)
    {
        for(int k=0;k<8;k++)
        {
            for(int j=0;j<8;j++)
            {
                attacked_squares[k][j] = false;
            }
        }

        this.myX = myX;
        this.myY = myY;
        // UP + RIGHT
        ChessboardLogicHelper.getDiagLineAttacks(myX, myY, 1, 1, cc, attacked_squares);
        // UP + LEFT
        ChessboardLogicHelper.getDiagLineAttacks(myX, myY, 1, -1, cc, attacked_squares);
        // DOWN + RIGHT
        ChessboardLogicHelper.getDiagLineAttacks(myX, myY, -1, 1, cc, attacked_squares);
        // DOWN + LEFT
        ChessboardLogicHelper.getDiagLineAttacks(myX, myY, -1, -1, cc, attacked_squares);

        return attacked_squares;
    }
}
