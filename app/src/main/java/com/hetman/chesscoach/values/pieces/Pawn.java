package com.hetman.chesscoach.values.pieces;

import android.os.Parcel;
import android.os.Parcelable;

import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.values.ChessPieces;

/**
 * Created by shevson on 4/15/17.
 */

public class Pawn extends ChessPiece
{
    boolean enpassantFlag;
    public Pawn(boolean color)
    {
        super(color);
        this.type = ChessPieces.PAWN;
    }

    public Pawn(Parcel in)
    {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeByte((byte) (color ? 1 : 0));
        dest.writeString(type.toString());
    }

    public static final Parcelable.Creator<Pawn> CREATOR = new Parcelable.Creator<Pawn>()
    {
        public Pawn createFromParcel(Parcel in)
        {
            return new Pawn(in) {
            };
        }

        public Pawn[] newArray(int size)
        {
            return new Pawn[size];
        }
    };

    @Override
    public boolean tryToMove(ChessboardController cc, int xfrom, int yfrom, int xto, int yto, boolean virtualMove)
    {
        boolean valid = false;

        // White pawn
        if(this.color)
        {
            if (xto - xfrom == 1 && yto == yfrom && cc.getSquare(xto, yto) == null)
            {
                // Single move
                if(xto == 7)
                {
                    if(!virtualMove)
                    {
                        cc.isPawnPromoting(xto, yto, this.color);
                    }
                }
                valid = true;
            }
            else if (xto - xfrom == 2 && xfrom == 1 && yto == yfrom && cc.getSquare(xto, yto) == null)
            {
                // Double move (from 2/7 line)
                valid = true;
                if(!virtualMove)
                {
                    enpassantFlag = true;
                    cc.setEnPassant(true);
                }
            }
            else if(xto - xfrom == 1 && Math.abs(yto - yfrom) == 1)
            {
                // Taking
                if(cc.getSquare(xto, yto)!=null)
                {
                    if(cc.getSquare(xto,yto).getColor()!=this.color)
                    {
                        if(xto == 7)
                        {
                            if(!virtualMove)
                            {
                                cc.isPawnPromoting(xto, yto, this.color);
                            }
                        }
                        valid = true;
                    }
                }

                if(yfrom-1>=0)
                {
                    if(cc.getSquare(xfrom, yfrom-1)!=null)
                    {
                        if(cc.getSquare(xfrom, yfrom-1).getColor()!=this.color)
                        {
                            ChessPiece piece = cc.getSquare(xfrom, yfrom-1);
                            if(piece instanceof Pawn)
                            {
                                Pawn p = (Pawn) piece;
                                if(p.getEnPassantFlag())
                                {
                                    valid = true;
                                    if(!virtualMove)
                                    {
                                        cc.didEnPassant(-1);
                                    }
                                }
                            }
                        }
                    }
                }

                if(yfrom+1<8)
                {
                    if(cc.getSquare(xfrom, yfrom+1)!=null)
                    {
                        if(cc.getSquare(xfrom, yfrom+1).getColor()!= this.color)
                        {
                            ChessPiece piece = cc.getSquare(xfrom, yfrom+1);
                            if(piece instanceof Pawn)
                            {
                                Pawn p = (Pawn) piece;
                                if(p.getEnPassantFlag())
                                {
                                    valid = true;
                                    if(!virtualMove)
                                    {
                                        cc.didEnPassant(1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                valid = false;
            }
        }
        // Black pawn
        else
        {
            if(xfrom - xto == 1 && yto == yfrom &&  cc.getSquare(xto, yto) == null)
            {
                // Single move
                if(xto == 0)
                {
                    if(!virtualMove)
                    {
                        cc.isPawnPromoting(xto, yto, this.color);
                    }
                }
                valid = true;
            }
            else if(xfrom - xto == 2 && xfrom == 6 && yto == yfrom && cc.getSquare(xto, yto) == null)
            {
                // Double move (from 2/7 line)
                valid = true;
                if(!virtualMove)
                {
                    enpassantFlag = true;
                    cc.setEnPassant(true);
                }
            }
            else if(xfrom - xto == 1 && Math.abs(yfrom - yto) == 1)
            {
                // Taking
                if(cc.getSquare(xto, yto)!=null)
                {
                    if(cc.getSquare(xto, yto).getColor()!= this.color)
                    {
                        if(xto == 0)
                        {
                            if(!virtualMove)
                            {
                                cc.isPawnPromoting(xto, yto, this.color);
                            }
                        }
                        valid = true;
                    }
                }

                if(yfrom-1>=0)
                {
                    if(cc.getSquare(xfrom, yfrom-1)!=null)
                    {
                        if(cc.getSquare(xfrom, yfrom-1).getColor()!= this.color)
                        {
                            ChessPiece piece = cc.getSquare(xfrom, yfrom-1);
                            if(piece instanceof Pawn)
                            {
                                Pawn p = (Pawn) piece;
                                if(p.getEnPassantFlag())
                                {
                                    valid = true;
                                    if(!virtualMove)
                                    {
                                        cc.didEnPassant(-1);
                                    }
                                }
                            }
                        }

                    }
                }

                if(yfrom+1<8)
                {
                    if(cc.getSquare(xfrom, yfrom+1)!=null)
                    {
                        if(cc.getSquare(xfrom, yfrom+1).getColor()!= this.color)
                        {
                            ChessPiece piece = cc.getSquare(xfrom, yfrom+1);
                            if(piece instanceof Pawn)
                            {
                                Pawn p = (Pawn) piece;
                                if(p.getEnPassantFlag())
                                {
                                    valid = true;
                                    if(!virtualMove)
                                    {
                                        cc.didEnPassant(1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                valid = false;
            }
        }
        if(valid && !virtualMove)
        {
            attackedByMe(xto, yto, cc);
        }
        return valid;
    }

    @Override
    public boolean [][] attackedByMe(int myX, int myY, ChessboardController cc)
    {
        for(int k=0;k<8;k++)
        {
            for(int j=0;j<8;j++)
            {
                attacked_squares[k][j] = false;
            }
        }

        this.myX = myX;
        this.myY = myY;

        if(this.color)
        {
            if(myX+1<8 && myY+1<8)
            {
                attacked_squares[myX+1][myY+1] = true;
            }
            if(myX+1<8 && myY-1 >=0)
            {
                attacked_squares[myX+1][myY-1] = true;
            }
        }
        else
        {
            if(myX-1 >= 0 && myY+1<8)
            {
                attacked_squares[myX-1][myY+1] = true;
            }
            if(myX-1 >= 0 && myY-1 >=0)
            {
                attacked_squares[myX-1][myY-1] = true;
            }
        }
        return attacked_squares;
    }

    public boolean getEnPassantFlag()
    {
        return enpassantFlag;
    }

    public void cleanEnPassantFlag()
    {
        enpassantFlag = false;
    }
}
