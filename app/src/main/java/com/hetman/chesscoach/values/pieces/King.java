package com.hetman.chesscoach.values.pieces;

import android.os.Parcel;
import android.os.Parcelable;

import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.values.ChessPieces;

/**
 * Created by shevson on 4/15/17.
 */

public class King extends ChessPiece
{
    boolean kingMoved = false;
    public King(boolean color)
    {
        super(color);
        this.type = ChessPieces.KING;
    }

    public King(Parcel in)
    {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeByte((byte) (color ? 1 : 0));
        dest.writeString(type.toString());
    }

    public static final Parcelable.Creator<King> CREATOR = new Parcelable.Creator<King>()
    {
        public King createFromParcel(Parcel in)
        {
            return new King(in) {
            };
        }

        public King[] newArray(int size)
        {
            return new King[size];
        }
    };

    @Override
    public boolean tryToMove(ChessboardController cc, int xfrom, int yfrom, int xto, int yto, boolean virtualMove)
    {
        boolean moveOK = false;
        attackedByMe(xfrom, yfrom, cc);
        if(attacked_squares[xto][yto])
        {
            if(cc.getSquare(xto, yto)==null)
            {
                moveOK = true;
            }
            else
            {
                if(cc.getSquare(xto, yto).getColor()!=this.color)
                {
                    moveOK = true;
                }
            }
        }


        if(!kingMoved)
        {
            if(this.color)
            {
                if(xfrom==0 && yfrom==4)
                {
                    if(xto==xfrom && yto==yfrom+2 && cc.getSquare(xfrom,yfrom+1)==null && cc.getSquare(xfrom, yfrom+2)==null)
                    {
                        if(!isUnderCheck(xfrom, yfrom, cc) && !isUnderCheck(xfrom, yfrom+1, cc) && !isUnderCheck(xfrom, yfrom+2, cc) )
                        {
                            if(cc.isCastleKingSideAvailable(this.color))
                            {
                                moveOK = true;
                                cc.kingWasCastled(this.color, true);
                            }
                        }
                    }
                    else if(xto==xfrom && yto==yfrom-2 && cc.getSquare(xfrom, yfrom-1)==null && cc.getSquare(xfrom, yfrom-2)==null && cc.getSquare(xfrom, yfrom-3)==null)
                    {
                        if(!isUnderCheck(xfrom, yfrom, cc) && !isUnderCheck(xfrom, yfrom-1, cc) && !isUnderCheck(xfrom, yfrom-2, cc) && !isUnderCheck(xfrom, yfrom-3,cc) )
                        {
                            if(cc.isCastleQueenSideAvailable(this.color))
                            {
                                moveOK = true;
                                cc.kingWasCastled(this.color, false);
                            }
                        }
                    }
                }
            }
            else
            {
                if(xfrom==7 && yfrom==4)
                {
                    if(xto==xfrom && yto==yfrom+2 && cc.getSquare(xfrom,yfrom+1)==null && cc.getSquare(xfrom, yfrom+2)==null)
                    {
                        if(!isUnderCheck(xfrom, yfrom, cc) && !isUnderCheck(xfrom, yfrom+1, cc) && !isUnderCheck(xfrom, yfrom+2, cc) )
                        {
                            if(cc.isCastleKingSideAvailable(this.color))
                            {
                                moveOK = true;
                                cc.kingWasCastled(this.color, true);
                            }
                        }
                    }
                    else if(xto==xfrom && yto==yfrom-2 && cc.getSquare(xfrom, yfrom-1)==null && cc.getSquare(xfrom, yfrom-2)==null && cc.getSquare(xfrom, yfrom-3)==null)
                    {
                        if(!isUnderCheck(xfrom, yfrom, cc) && !isUnderCheck(xfrom, yfrom-1, cc) && !isUnderCheck(xfrom, yfrom-2, cc) && !isUnderCheck(xfrom, yfrom-3,cc) )
                        {
                            if(cc.isCastleQueenSideAvailable(this.color))
                            {
                                moveOK = true;
                                cc.kingWasCastled(this.color, false);
                            }
                        }
                    }
                }
            }
        }


        if(moveOK && !kingMoved)
        {
            kingMoved = true;
            cc.setCastleKingSideAvailable(false,this.color);
            cc.setCastleQueenSideAvailable(false,this.color);
        }

        if(moveOK && !virtualMove)
        {
            attackedByMe(xto, yto, cc);
        }
        return moveOK;
    }

    @Override
    public boolean [][] attackedByMe(int myX, int myY, ChessboardController cc)
    {
        for(int k=0;k<8;k++)
        {
            for(int j=0;j<8;j++)
            {
                attacked_squares[k][j] = false;
            }
        }
        this.myX = myX;
        this.myY = myY;

        if(myX >= 0 && myX < 8 && myY >=0 && myY < 8)
        {
            if(myX+1 < 8)
            {
                attacked_squares[myX+1][myY] = true;
                if(myY+1 < 8)
                {
                    attacked_squares[myX+1][myY+1] = true;
                    attacked_squares[myX][myY+1] = true;
                }
                if(myY-1 >= 0 )
                {
                    attacked_squares[myX+1][myY-1] = true;
                    attacked_squares[myX][myY-1] = true;
                }
            }

            if(myX-1 >= 0)
            {
                attacked_squares[myX-1][myY] = true;
                if(myY+1 < 8)
                {
                    attacked_squares[myX-1][myY+1] = true;
                }
                if(myY-1 >= 0)
                {
                    attacked_squares[myX-1][myY-1] = true;
                }
            }

            if(myY-1 >= 0)
            {
                attacked_squares[myX][myY-1] = true;
            }

            if(myY+1 < 8)
            {
                attacked_squares[myX][myY+1] = true;
            }
        }

        return attacked_squares;
    }

    private boolean isUnderCheck(int x, int y, ChessboardController cc)
    {
        boolean underCheck = false;
        boolean [][] attacked;
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(cc.getSquare(i,j)!=null)
                {
                    if(cc.getSquare(i,j).getColor()!=this.color)
                    {
                        attacked = cc.getSquare(i,j).attackedByMe(i,j,cc);
                        for(int a=0; a<8; a++)
                        {
                            for(int b=0; b<8; b++)
                            {
                                if(attacked[a][b]==true && x==a && y==b)
                                {
                                    underCheck = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return underCheck;
    }
}
