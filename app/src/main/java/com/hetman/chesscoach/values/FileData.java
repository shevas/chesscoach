package com.hetman.chesscoach.values;

import com.hetman.chesscoach.tools.PgnHelper;

import java.io.File;

/**
 * Created by shevson on 5/19/17.
 */

public class FileData
{
    // Basic
    public String filename;
    public File file;

    // Additional
    public String level = "";
    public FileData(String filename, File file)
    {
        this.filename = filename;
        this.file = file;
        extractLevel();
    }

    private void extractLevel()
    {
        // READ FILE
        String fullPgn = "";
        fullPgn = PgnHelper.readPGNFile(this.file);

        // READ LEVEL FROM PGN
        int i = fullPgn.indexOf("[Level \"")+8;
        for(int k=i; k<fullPgn.length(); k++)
        {
            char c = fullPgn.charAt(k);
            if(c=='\"')
            {
                break;
            }
            level = level + c;
        }
    }
}
