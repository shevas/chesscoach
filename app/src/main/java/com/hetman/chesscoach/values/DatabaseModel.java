package com.hetman.chesscoach.values;

/**
 * Created by shevson on 6/3/17.
 */

public class DatabaseModel
{
    private long id;
    private String name;
    private String onMove;
    private int moveNr;
    private String question;
    private String hint;
    private String level;
    private String answer;
    private String timer;
    private String fen;
    private String pgn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOnMove() {
        return onMove;
    }

    public void setOnMove(String onMove) {
        this.onMove = onMove;
    }

    public int getMoveNr() {
        return moveNr;
    }

    public void setMoveNr(int moveNr) {
        this.moveNr = moveNr;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getFen() {
        return fen;
    }

    public void setFen(String fen) {
        this.fen = fen;
    }

    public String getPgn() {
        return pgn;
    }

    public void setPgn(String pgn) {
        this.pgn = pgn;
    }
}
