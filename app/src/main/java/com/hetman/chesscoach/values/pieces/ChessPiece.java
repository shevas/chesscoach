package com.hetman.chesscoach.values.pieces;

import android.os.Parcel;
import android.os.Parcelable;

import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.values.ChessPieces;

/**
 * Created by shevson on 4/15/17.
 */

public abstract class ChessPiece implements Parcelable, Cloneable
{
    protected boolean color;
    protected ChessPieces type;

    protected int myX = -1;
    protected int myY = -1;
    protected boolean [][] attacked_squares = new boolean [8][8];

    public ChessPiece(boolean color)
    {
        this.color = color;
    }

    public ChessPiece(Parcel in)
    {
        this.color = in.readByte()==1?true:false;
        String type = in.readString();
        this.type = ChessPieces.getChessPiecesByName(type);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public ChessPieces getType()
    {
        return this.type;
    }

    public boolean getColor()
    {
        return this.color;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeByte((byte) (color ? 1 : 0));
        dest.writeString(type.toString());
    }

    public static final Parcelable.Creator<ChessPiece> CREATOR = new Parcelable.Creator<ChessPiece>()
    {
        public ChessPiece createFromParcel(Parcel in)
        {
            return new ChessPiece(in) {
            };
        }

        public ChessPiece[] newArray(int size)
        {
            return new ChessPiece[size];
        }
    };

    public boolean tryToMove(ChessboardController cc, int xfrom, int yfrom, int xto, int yto, boolean virtualMove)
    {
        return true;
    }

    public boolean [][] attackedByMe(int myX, int myY, ChessboardController cc)
    {
        return attacked_squares;
    }
}
