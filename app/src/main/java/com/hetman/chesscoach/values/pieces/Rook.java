package com.hetman.chesscoach.values.pieces;

import android.os.Parcel;
import android.os.Parcelable;

import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.tools.ChessboardLogicHelper;
import com.hetman.chesscoach.values.ChessPieces;

/**
 * Created by shevson on 4/15/17.
 */

public class Rook extends ChessPiece
{
    boolean rookMoved = false;
    public Rook(boolean color)
    {
        super(color);
        this.type = ChessPieces.ROOK;
    }

    public Rook(Parcel in)
    {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeByte((byte) (color ? 1 : 0));
        dest.writeString(type.toString());
    }

    public static final Parcelable.Creator<Rook> CREATOR = new Parcelable.Creator<Rook>()
    {
        public Rook createFromParcel(Parcel in)
        {
            return new Rook(in) {
            };
        }

        public Rook[] newArray(int size)
        {
            return new Rook[size];
        }
    };

    @Override
    public boolean tryToMove(ChessboardController cc, int xfrom, int yfrom, int xto, int yto, boolean virtualMove)
    {
        boolean moveOK = false;
        attackedByMe(xfrom, yfrom, cc);
        if(attacked_squares[xto][yto])
        {
            if(cc.getSquare(xto, yto)==null)
            {
                moveOK = true;
            }
            else
            {
                if(cc.getSquare(xto, yto).getColor()!=this.color)
                {
                    moveOK = true;
                }
            }
        }

        if(!rookMoved && moveOK)
        {
            if(this.color)
            {
                if(xfrom==0 && yfrom==0)
                {
                    cc.setCastleQueenSideAvailable(false, this.color);
                }
                if(xfrom==0 && yfrom==7)
                {
                    cc.setCastleKingSideAvailable(false, this.color);
                }
            }
            else
            {
                if(xfrom==7 && yfrom==0)
                {
                    cc.setCastleQueenSideAvailable(false, this.color);
                }
                if(xfrom==7 && yfrom==7)
                {
                    cc.setCastleKingSideAvailable(false, this.color);
                }
            }
            rookMoved = true;
        }
        if(moveOK && !virtualMove)
        {
            attackedByMe(xto, yto, cc);
        }
        return moveOK;
    }

    @Override
    public boolean [][] attackedByMe(int myX, int myY, ChessboardController cc)
    {
        for(int k=0;k<8;k++)
        {
            for(int j=0;j<8;j++)
            {
                attacked_squares[k][j] = false;
            }
        }
        this.myX = myX;
        this.myY = myY;
        // UP
        ChessboardLogicHelper.getDiagLineAttacks(myX, myY, 1, 0, cc, this.attacked_squares);
        // DOWN
        ChessboardLogicHelper.getDiagLineAttacks(myX, myY, -1, 0, cc, this.attacked_squares);
        // RIGHT
        ChessboardLogicHelper.getDiagLineAttacks(myX, myY, 0, 1, cc, this.attacked_squares);
        // LEFT
        ChessboardLogicHelper.getDiagLineAttacks(myX, myY, 0, -1, cc, this.attacked_squares);
        return attacked_squares;
    }
}
