package com.hetman.chesscoach.values;

import com.hetman.chesscoach.values.pieces.ChessPiece;

/**
 * Created by shevson on 3/11/17.
 */

public enum ChessPieces
{
    PAWN("pion"),
    KNIGHT("skoczek"),
    BISHOP("goniec"),
    ROOK("wieża"),
    QUEEN("hetman"),
    KING("król"),
    NULLPIECE(null);

    private final String name;

    ChessPieces(String name)
    {
        this.name = name;
    }

   public static final ChessPieces getChessPiecesByName(String name)
   {
       for(ChessPieces chessPieces : ChessPieces.values())
       {
           if(chessPieces.toString().equals(name))
           {
               return chessPieces;
           }
       }
       return NULLPIECE;
   }

    @Override
    public String toString()
    {
        return name;
    }
}
