package com.hetman.chesscoach.values.pieces;

import android.os.Parcel;
import android.os.Parcelable;

import com.hetman.chesscoach.chessboard.ChessboardController;
import com.hetman.chesscoach.values.ChessPieces;

/**
 * Created by shevson on 4/15/17.
 */

public class Knight extends ChessPiece
{
    public Knight(boolean color)
    {
        super(color);
        this.type = ChessPieces.KNIGHT;
    }

    public Knight(Parcel in)
    {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeByte((byte) (color ? 1 : 0));
        dest.writeString(type.toString());
    }

    public static final Parcelable.Creator<Knight> CREATOR = new Parcelable.Creator<Knight>()
    {
        public Knight createFromParcel(Parcel in)
        {
            return new Knight(in) {
            };
        }

        public Knight[] newArray(int size)
        {
            return new Knight[size];
        }
    };

    @Override
    public boolean tryToMove(ChessboardController cc, int xfrom, int yfrom, int xto, int yto, boolean virtualMove)
    {
        boolean moveOK = false;
        attackedByMe(xfrom, yfrom, cc);

       if(attacked_squares[xto][yto])
       {
           if(cc.getSquare(xto,yto)==null)
           {
               moveOK = true;
           }
           else
           {
               if(cc.getSquare(xto,yto).getColor()!=this.getColor())
               {
                   moveOK = true;
               }
           }
       }
       if(moveOK && !virtualMove)
       {
           attackedByMe(xto, yto, cc);
       }
       return moveOK;
    }

    @Override
    public boolean [][] attackedByMe(int myX, int myY, ChessboardController cc)
    {
        for(int k=0;k<8;k++)
        {
            for(int j=0;j<8;j++)
            {
                attacked_squares[k][j] = false;
            }
        }
        this.myX = myX;
        this.myY = myY;

        if(myX+2 < 8)
        {
            // 2 UP 1 RIGHT
            if(myY+1 < 8)
            {
                attacked_squares[myX+2][myY+1] = true;
            }
            // 2 UP 1 LEFT
            if(myY-1 >= 0)
            {
                attacked_squares[myX+2][myY-1] = true;
            }

        }
        if(myX-2 >= 0)
        {
            // 2 DOWN 1 RIGHT
            if(myY+1 < 8)
            {
                attacked_squares[myX-2][myY+1] = true;
            }
            // 2 DOWN 1 LEFT
            if(myY-1 >= 0)
            {
                attacked_squares[myX-2][myY-1] = true;
            }
        }
        if(myX+1 < 8)
        {
            // 1 UP 2 RIGHT
            if(myY+2 < 8)
            {
                attacked_squares[myX+1][myY+2] = true;
            }
            // 1 UP 2 LEFT
            if(myY-2 >= 0)
            {
                attacked_squares[myX+1][myY-2] = true;
            }
        }
        if(myX-1 >= 0)
        {
            // 1 DOWN 2 RIGHT
            if(myY+2 < 8)
            {
                attacked_squares[myX-1][myY+2] = true;
            }
            // 1 DOWN 2 LEFT
            if(myY-2 >= 0)
            {
                attacked_squares[myX-1][myY-2] = true;
            }
        }
        return attacked_squares;
    }
}
